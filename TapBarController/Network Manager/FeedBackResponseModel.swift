//
//  FeedBackResponseModel.swift
//  Apll
//
//  Created by Shiva Ganesh on 09/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation
import EVReflection


class FeedBackResponseModel: EVNetworkingObject,Codable {
    
    var response:String?
    var error: String?
    var errorMsg: String?
     
    convenience init(response:String, error: String, errorMsg:String) {
        
        self.init()
        self.error = error
        self.errorMsg = errorMsg
        self.response = response
    }
}
