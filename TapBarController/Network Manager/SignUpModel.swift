//
//  SignUpModel.swift
//  Apll
//
//  Created by synycs on 27/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation
import EVReflection


class SignUpModel:EVNetworkingObject{
    
    var dateOfBirth:String?
    var email:String?
    var invoiceNumber:String?
    var mobileNumber:String?
    var orderNumber:String?
    var password:String?
    var studentIdentityNumber:String?
    
    convenience init(dateOfBirth:String,email:String,invoiceNumber:String,mobileNumber:String,orderNumber:String,password:String,studentIdentityNumber:String) {
       
        self.init()
        
        self.dateOfBirth = dateOfBirth
        self.email = email
        self.invoiceNumber = invoiceNumber
        self.mobileNumber = mobileNumber
        self.orderNumber = orderNumber
        self.password = password
        self.studentIdentityNumber = studentIdentityNumber
      
    }
    
    
}




