//
//  LoginResponceModel.swift
//  Apll
//
//  Created by synycs on 28/12/18.
//  Copyright © 2018 synycs. All rights reserved.
 

import Foundation
import EVReflection
class   LoginResponceModel: EVNetworkingObject {
    
    var  user = userModel()
    var  access_token:String?
    var  expires_in:String?
    
    convenience init(user:userModel,access_token:String,expires_in:String) {
        self.init()
        self.user = user
        self.access_token = access_token
        self.expires_in =  expires_in
        
    }
}
class userModel:EVNetworkingObject {
    var employeeId:String?
    var userName:String?
    var profilePic:String?
    var roles = [String]()
    var studentBatchEnrolledDate:String?
    
}

