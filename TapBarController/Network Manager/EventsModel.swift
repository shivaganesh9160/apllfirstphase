//
//  EventsModel.swift
//  Apll
//
//  Created by synycs on 11/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation
import EVReflection


class  eventID:EVNetworkingObject {
    
    var id:String?
}


class  responseSt:EVNetworkingObject {
    
    var eventId = eventID()
    var eventname:String = "String"
    var location:String = "String"
    var eventDescription:String = "String"
    var image:String = "String"
    var eventDate:String = "String"
    var eventCreatedDate:String = "String"
    var eventUpdateDate:String = "String"
    
    override func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
        return [("eventDescription","description")]
    }
    
    
}

class  errorSt:EVNetworkingObject {
    
    var error:String = "String"
    
}

class  errorMsgSt:EVNetworkingObject {
    
    var errorMsg:String?
}

class EventsModel:EVNetworkingObject {
     
    var response = [responseSt()]
    var error = errorSt()
    var errorMsg = errorMsgSt()
    
    convenience init(response:[responseSt],error:errorSt,errorMsg:errorMsgSt) {
        
        self.init()
        self.response = response
        self.error = error
        self.errorMsg = errorMsg
        
    }
    
}
