//
//  FeedBackModel.swift
//  Apll
//
//  Created by Shiva Ganesh on 08/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation
import EVReflection

struct studentFeedBackDetails {
    var id:String?
}
 
struct studentFeedbackDetailsD {
    var feedback:String?
    var question:String?
    var studentFeedBackDetailsId = studentFeedBackDetails()
}
 class  FeedBackModel:EVNetworkingObject {
    
    var courseId:String = "String"
    var courseName:String = "String"
    var nameOfStudent:String = "String"
    var studentComment:String = "String"
    var studentFeedbackId = studentFeedBackDetails()
    var studentFeedbackDetailsDto = [studentFeedbackDetailsD()]
    var studentId:String = "String"
    var feedbackType:String = "String"
    convenience init(courseId:String, courseName:String, nameOfStudent:String, studentComment:String,studentFeedbackDetailsD:[studentFeedbackDetailsD],studentFeedbackId:studentFeedBackDetails, studentId:String,feedbackType:String) {
        
        self.init()
        self.courseId = courseId
        self.courseName = courseName
        self.nameOfStudent = nameOfStudent
        self.studentComment = studentComment
        self.studentFeedbackDetailsDto = studentFeedbackDetailsD
        self.studentId = studentId
        self.studentFeedbackId = studentFeedbackId
        self.feedbackType = feedbackType
    }
    
}









