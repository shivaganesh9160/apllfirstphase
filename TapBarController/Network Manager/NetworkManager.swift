//
//  NetworkManager.swift
//  Apll
//
//  Created by Shiva Ganesh on 08/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import UIKit

class  NetWorkingManager {
     
    let headerForFeedBack = ["Content-Type":"Application/json"]
    static func sendFeedBack(feedback:FeedBackModel, listener: @escaping ((result:Any, success:Bool)) -> ()) {
        if let sendFeedbackModel = feedback.toDictionary() as? Dictionary<String,Any> {
            Alamofire.request(API.FEEDBACK_API, method:.post , parameters:sendFeedbackModel, encoding:JSONEncoding.default ).responseObject{ (response:DataResponse<FeedBackResponseModel>) in
                if let result = response.result.value {
                    print("result \(result)")
                    
                    
                      listener((result:result, success: true))
                }
            }
        }
    }
    
    let headerForProfile = ["Content-Type":"Application/json"]
    static func  profileModelFunc(SignUpRequest:ProfileImgUploadModel, listener: @escaping ((result:Any, success:Bool)) -> ()) {
        
        
        if let  sendSignUpModel = SignUpRequest.toDictionary() as? Dictionary<String,Any> {
            Alamofire.request(API.PROFILE_IMG_UPLOAD, method:.put , parameters:sendSignUpModel, encoding:JSONEncoding.default ).responseObject{ (response:DataResponse<FeedBackResponseModel>) in
                if let result = response.result.value {
                    print("result \(result)")
                    
                    listener((result:result, success: true))
                    
                }
            }
        }
    }
    
    let  ForgotHeader = ["Content-Type":"Application/json"]
    static func   forgotPassword(forgotPasswordModel:ForgotPasswordModel, listener: @escaping ((result:Any, success:Bool)) -> ()) {
        
        
        if let  sendForgotModel = forgotPasswordModel.toDictionary() as? Dictionary<String,Any> {
            Alamofire.request(API.FORGOTPASSWORD, method:.post , parameters:sendForgotModel, encoding:JSONEncoding.default ).responseObject{ (response:DataResponse<FeedBackResponseModel>) in
                if let result = response.result.value {
                    print("result \(result)")
                    
                    listener((result:result, success: true))
                    
                }
            }
        }
    }
    
    
    
    
    
        let headerForEvents = ["Content-Type":"Application/json"]
        static func   eventsModelFunc(SignUpRequest:EventsModel, listener: @escaping ((result:Any, success:Bool)) -> ()) {
            if let  sendSignUpModel = SignUpRequest.toDictionary() as? Dictionary<String,Any> {
                Alamofire.request(API.EVENTS_API, method:.get , parameters:sendSignUpModel, encoding:JSONEncoding.default ).responseObject{ (response:DataResponse<EventsModel>) in
                    if let result = response.result.value {
    
                         listener((result:result,success:true))
    
                        
                        print(result)
                        
                    }
                }
            }
        }
    
    static func eventsModelFunc(listener:@escaping ((result:Any, success:Bool))->()) {
        Alamofire.request(API.EVENTS_API, method:.get,encoding:JSONEncoding.default).responseObject { (response:DataResponse<EventsModel>) in
            if let result = response.result.value {
                listener((result:result, success: true))
                
                
                
                
            }
        }
    }
    
    
    
        let headerForLogin = ["Content-Type":"Application/json"]
    static func  sendLogInModel(sendLogin:LogInModel, listener: @escaping ((result:Any, success:Bool)) -> ()) {
        if let  sendLogInModel = sendLogin.toDictionary() as? Dictionary<String,Any> {
            Alamofire.request(API.LOGIN, method:.post , parameters:sendLogInModel, encoding:JSONEncoding.default ).responseObject{ (response:DataResponse<LoginResponceModel>) in
                if let result = response.result.value {
                    listener((result:result,success:true))
                } else {
                    
                    SingleToneClass.shared.dismissProgressLoading()
                    print("Login Failed")
                    let alertController = UIAlertController(title: "Login Failed", message: "Invalid User ID / Password", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                    alertController.presentInOwnWindow(animated: true, completion: {
                        
                        
                        print("Alert Shown")
                    })
                }
            }
        }
    }
    
    
    let headerForSignUp = ["Content-Type":"Application/json"]
    static func  signUpModelFunc(SignUpRequest:SignUpModel, listener: @escaping ((result:Any, success:Bool)) -> ()) {
        if let  sendSignUpModel = SignUpRequest.toDictionary() as? Dictionary<String,Any> {
            Alamofire.request(API.SIGN_UP, method:.put , parameters:sendSignUpModel, encoding:JSONEncoding.default ).responseObject{ (response:DataResponse<FeedBackResponseModel>) in
                if let result = response.result.value {
                    print("result \(result)")
                    listener((result:result,success:true))
                }else {
                    
                    let alertController = UIAlertController(title: "Sign Up Failed", message: "Invalid Details", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                    alertController.presentInOwnWindow(animated: true, completion: {
                        print("Alert Shown")
                    })
                }
            }
        }
    }
   
}
extension UIAlertController {
    func presentInOwnWindow(animated: Bool, completion: (() -> Void)?) {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(self, animated: animated, completion: completion)
    }
    
}

