//
//  NetworkConstants.swift
//  Apll
//
//  Created by Shiva Ganesh on 08/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation

struct  API {
     
    static let BASE_API = "http://174.138.121.216/student"
    static let EVENTS_API = "\(BASE_API)/event"
    static let FEEDBACK_API = "\(BASE_API)/createStudentFeedback"
    static let SIGN_UP = "\(BASE_API)/student/studentSignUp"
    static let LOGIN = "http://174.138.121.216/auth/auth/login"
    static let  PROFILE_IMG_UPLOAD = "\(BASE_API)/student/updateProfilePic"
    static let  FORGOTPASSWORD =  "\(BASE_API)/student/verifyOTP"
 
}
 



