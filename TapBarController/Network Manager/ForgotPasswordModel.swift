//
//  ForgotPasswordModel.swift
//  Apll
//
//  Created by synycs on 05/01/19.
//  Copyright © 2019 synycs. All rights reserved.
//

import Foundation
import EVReflection

class ForgotPasswordModel:EVNetworkingObject{
    
    var  studentIdentityNumber:String?
    var  otp:String?
    var  password:String?
    
    convenience init(studentIdentityNumber:String,otp:String,password:String) {
        
        self.init()
        self.studentIdentityNumber = studentIdentityNumber
        self.otp = otp
        self.password = password
        
    }
   
}

