//
//  ProfileImgUploadModel.swift
//  Apll
//
//  Created by Shiva Ganesh on 01/01/19.
//  Copyright © 2019 synycs. All rights reserved.
//

import Foundation
import EVReflection

class  ProfileImgUploadModel: EVNetworkingObject {
     
    var studentIdentityNumber:String?
    var studentProfilePic:String?
    
    convenience init(studentIdentityNumber:String,studentProfilePic:String) {
        
        self.init()
        self.studentIdentityNumber = studentIdentityNumber
        self.studentProfilePic = studentProfilePic
        
    }
    
    
    
    
}

