//
//  SingleToneClass.swift
//  Clousers181
//
//  Created by Saikumar on 26/06/18.
//  Copyright © 2018 SSApps. All rights reserved.
//

import UIKit


class SingleToneClass: NSObject {

    static let shared = SingleToneClass()
    
    override init() {
        
    }
    
    
    func showValidationAlert(target : UIViewController, title : String, message : String, OntapOkButton : @escaping (() -> Void))  {
        
        let emailAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (okayAction) in
            
            OntapOkButton()
            
        }
        emailAlert.addAction(okAction)
        DispatchQueue.main.async {
            target.present(emailAlert, animated: true, completion: nil)
        }
        
    }
    
    
   
    
}
