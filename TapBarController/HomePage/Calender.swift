//
//  Calender.swift
//  TapBarController
//
//  Created by synycs on 29/10/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit
import UICircularProgressRing


enum MyTheme {
    case light
    case dark
}
class  Calender: UIViewController,UICircularProgressRingDelegate {
    
    @IBOutlet weak var yearlyView: UICircularProgressRing!
    @IBOutlet weak var monthlyView: UICircularProgressRing!
    
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated:true) {
            
        }
        
    }
    
   override func viewDidAppear(_ animated: Bool)
    
             {
                self.monthlyView.startProgress(to: 90, duration:3.0)
                print("Done animating!")
                self.monthlyView.isClockwise = true
                self.monthlyView.startAngle = -CGFloat.pi/2
                self.monthlyView.endAngle = 90
                
                
                self.yearlyView.startProgress(to: 90, duration: 3.0)
                self.yearlyView.isClockwise =  true
        
    }
    
    var theme = MyTheme.dark
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let progressRing = monthlyView
        let yearlyRing = yearlyView
         
        yearlyRing?.maxValue =  100
        yearlyRing?.outerRingColor = UIColor.clear
    
        yearlyRing?.innerRingColor = ColorsForViews.thirdBlue
        yearlyRing?.innerRingWidth = 7
        
        progressRing?.maxValue = 100
        progressRing?.outerRingColor = UIColor.clear
        progressRing?.innerRingColor = ColorsForViews.thirdBlue
        progressRing?.innerRingWidth = 7
        progressRing?.startAngle = -CGFloat.pi/2
        progressRing?.endAngle = 95
        
        self.title = "My Calender"
        self.navigationController?.navigationBar.isTranslucent=false
        self.view.backgroundColor=UIColor.white
        
        view.addSubview(calenderView)
        calenderView.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive=true
        calenderView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive=true
        calenderView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant:365).isActive=true
        
        let rightBarBtn = UIBarButtonItem(title: "Light", style: .plain, target: self, action: #selector(rightBarBtnAction))
        self.navigationItem.rightBarButtonItem = rightBarBtn
    }
     
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        calenderView.myCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    @objc func rightBarBtnAction(sender: UIBarButtonItem) {
        if theme == .dark {
            sender.title = "Dark"
            theme = .light
            Style.themeLight()
        } else {
            sender.title = "Light"
            theme = .dark
            Style.themeDark()
        }
        self.view.backgroundColor=Style.bgColor
        calenderView.changeTheme()
    }
    
    let calenderView: CalenderView = {
        let v=CalenderView(theme: MyTheme.light)
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
}()
    
    }
    



