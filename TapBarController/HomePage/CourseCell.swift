//
//  CourseCell.swift
//  Apll
//
//  Created by synycs on 02/03/19.
//  Copyright © 2019 synycs. All rights reserved.
//

import UIKit

class CourseCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var numberOfUnits: UILabel!
    
    
    @IBOutlet weak var unitNumberLabel: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
