
//  Terms&Conditions.swift
//  TapBarController
//
//  Created by synycs on 27/10/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit

class Terms_Conditions: UIViewController,UITextViewDelegate {

    @IBOutlet weak var contentLabel: UILabel!{
        didSet {
            acceptBtn1.isHidden =  false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
 
//        if self.view.frame.height > conditionsScrollView.frame.height {
//            acceptBtn1.isHidden = false
//            contentLabel.isHidden = true
//        } else {
//            acceptBtn1.isHidden = true
//            contentLabel.isHidden = false
//        }
        acceptBtn1.isHidden = true
        
    }
    
    @IBOutlet weak var acceptBtn1: UIButton! 
    
    
    @IBOutlet weak var scrollViewC: UIScrollView!
    
    @IBOutlet weak var conditionsScrollView: UITextView! {
        didSet {
             conditionsScrollView.delegate = self
        }
    }
    
    
    @IBAction func AcceptBtnAct(_ sender: UIButton) {
        
        
        var eventsRef =  Tab()
         
        eventsRef =  self.storyboard?.instantiateViewController(withIdentifier: "Tab") as!  Tab
        
        self.present(eventsRef, animated: true) {
            
            
        }
        
    }
    
    override func viewDidLayoutSubviews() {
         conditionsScrollView.setContentOffset(CGPoint.zero, animated: true)
    }

    
        @IBOutlet weak var textView: UITextView!
        @IBOutlet weak var button: UIButton!
        
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        acceptBtn1.isHidden = scrollView.contentOffset.y + scrollView.bounds.height < scrollView.contentSize.height
        
        if acceptBtn1.isHidden == false{
            
            contentLabel.isHidden = true
            
        }else{
            contentLabel.isHidden = false
            
        }
        
        
    }
        
        
//        if acceptBtn1.isHidden == false{
//
//            contentLabel.isHidden = true
//
//        }else{
//            contentLabel.isHidden = false
//
//        }
        
        
    }
    
    
    
    

    
