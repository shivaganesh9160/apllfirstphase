//
//  SignUpPage.swift
//  Apll
//
//  Created by synycs on 26/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit

class SignUpPage: UIViewController,UITextFieldDelegate {
     
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBOutlet weak var emailTF: TextStyle!
    @IBOutlet weak var invoiceTF: UITextField!
    @IBOutlet weak var orderNumTF: UITextField!
    @IBOutlet weak var studentIDTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
     
    @IBAction func pickerDateAct(_ sender: UITextField) {
       
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
         dobTF.text = dateFormatter.string(from: sender.date)
    }
    
    
    @IBAction func submitBtn(_ sender: Any) {
        
        if (studentIDTF.text?.count)! <= 1 {
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Student ID") {
                
                self.studentIDTF.becomeFirstResponder()
                
            }
            
        }else if (dobTF.text?.count)! <= 1 {
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Date of Birth") {
                
                self.dobTF.becomeFirstResponder()
            }
            
        }else if !mobileNumTF.isValidMobileNumberTextField() {
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Mobile Number") {
                
                self.mobileNumTF.becomeFirstResponder()
            }
            
        }else if !emailTF.isValidEmailTextField() {
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Email Address") {
                
                self.emailTF.becomeFirstResponder()
            }
            
        }else if (invoiceTF.text?.count)! <= 1 {
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Invoice Number") {
                
                self.invoiceTF.becomeFirstResponder()
            }
            
        }else if (orderNumTF.text?.count)! <= 1 {
            
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Order Number") {
                
                self.orderNumTF.becomeFirstResponder()
            }
            
        }else if (passwordTF.text?.count)! <= 1 {
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Enter Password") {
                
                self.passwordTF.becomeFirstResponder()
            }
            
        }else {
            
            signUpRef.invoiceNumber = invoiceTF.text!
            signUpRef.orderNumber = orderNumTF.text!
            signUpRef.studentIdentityNumber = studentIDTF.text!
            signUpRef.password = passwordTF.text!
             
            let does =  dobTF.text!
            print(does)
            
            let myDate = does
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "d MMM yyyy"
            let date = dateFormatter.date(from:myDate)!
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from:date)

            print(dateString)
            
            signUpRef.dateOfBirth =  dateString
            signUpRef.mobileNumber = mobileNumTF.text!
            signUpRef.email = emailTF.text!
            
            print(signUpRef)
            SingleToneClass.shared.showProgressLoading(title: "Signing Up Please Wait")
            NetWorkingManager.signUpModelFunc(SignUpRequest:signUpRef, listener: { (result,success) in
                
                let result  = result as? FeedBackResponseModel
                 
//                let resultBool = result?.error
//                print(resultBool!)
//                
//                let message = result?.errorMsg
                if  result?.error == "0" {
                    
                    print(result?.error as Any)
                    
                    SingleToneClass.shared.dismissProgressLoading()
                    
                    SingleToneClass.shared.showValidationAlert(target: self, title: "Successfully Submitted.", message: "") {
                        
                        self.dismiss(animated: true, completion: {
                            
                        })
                        
                    }
                    
                }else{
                    
                    
                      SingleToneClass.shared.dismissProgressLoading()
                    
                    if let errorMsg = result?.errorMsg!{
                        SingleToneClass.shared.showValidationAlert(target: self, title: "\(errorMsg)" , message: "Failed!") {
                        
                        self.studentIDTF.becomeFirstResponder()
                        
                    }
                    
                    }else{
                        
                        
                        
                        print(Error.self)
                    }
                }
                
            })
            
        }
       
    }
    
    
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
         textField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        
        return true;
    }
    
    
    
    var signUpRef = SignUpModel()
    
    var errorMsgRef = FeedBackResponseModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if SingleToneClass.shared.isInternetAvailable()==false{
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "No Internet Connection", message:  "") {
                
            }
         
        }
        
         
        self.hideKeyboardWhenTappedAround()
       dobTF.delegate = self
    }
    
}
