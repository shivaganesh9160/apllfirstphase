//
//  ForgotPass.swift
//  Apll
//
//  Created by synycs on 05/01/19.
//  Copyright © 2019 synycs. All rights reserved.
//

import UIKit

class ForgotPass: UIViewController {
 
    @IBOutlet weak var studentIDTF: TextStyle!
    @IBOutlet weak var OTPTF: TextStyle!
    @IBOutlet weak var NewPasswordTF: TextStyle!
    @IBOutlet weak var sendOTPOut: UIButton!
    @IBOutlet weak var submitBtnOut: UIButton!
    @IBOutlet weak var resendOTPOut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        if SingleToneClass.shared.isInternetAvailable()==false{
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "No Internet Connection", message:  "") {
                
            }
        
        }
        
        self.OTPTF.isHidden = true
        self.NewPasswordTF.isHidden = true
        self.submitBtnOut.isHidden = true
        self.resendOTPOut.isHidden = true
        
    }
    
    @IBAction func sendOTPAction(_ sender: Any) {
        
        
        if (studentIDTF.text?.count)! >= 2 {
           
            
            SingleToneClass.shared.showProgressLoading(title: "Sending OTP...")
            
        Service.shared.GETService(extraParam:"/student/forgetPassword/\(studentIDTF.text!)") { (responce) -> (Void) in
            
            print(responce)
            
            let responceref = responce as?  NSDictionary
            
            let errorMsg = responceref?["error"] as?  Bool
            
            print(errorMsg ??  "error not found")
            
            if  errorMsg  ==  false {
            
            
                SingleToneClass.shared.dismissProgressLoading()
                SingleToneClass.shared.showValidationAlert(target: self, title: "OTP Sent", message:"Check Registered Email For OTP", OntapOkButton: {
                    
//                    self.studentIDTF.text?.removeAll()
                    self.sendOTPOut.isHidden = true
                    self.OTPTF.isHidden = false
                    self.submitBtnOut.isHidden = false
                    self.NewPasswordTF.isHidden = false
                   
                    self.OTPTF.becomeFirstResponder()
                    
                    //        self.resendOTPOut.isHidden = false
                    
                })
               
            }
            else{

                 
                SingleToneClass.shared.dismissProgressLoading()
                SingleToneClass.shared.showValidationAlert(target: self, title: "Invalid User ID", message: "", OntapOkButton: {
                self.studentIDTF.text?.removeAll()
                self.studentIDTF.becomeFirstResponder()


                })

            }
         
        }
        
        
    }else{
    
            SingleToneClass.shared.showValidationAlert(target: self, title: "Enter Student ID", message:"") {
                
            }
     
    }
    
        }
   
    @IBAction func submitBtnAction(_ sender: Any) {
        
        let forgorRef = ForgotPasswordModel()
        
        if (studentIDTF.text?.count)! >= 2 {
         
            forgorRef.studentIdentityNumber = self.studentIDTF.text!
            forgorRef.otp = self.OTPTF.text!
            forgorRef.password = self.NewPasswordTF.text!
            
        NetWorkingManager.forgotPassword(forgotPasswordModel: forgorRef) { (result,success) in
            
            
            print(forgorRef)
            
            let resultFromServer = result as? FeedBackResponseModel
            
            
            print(resultFromServer!)
            
            if  resultFromServer?.error == "0" {
                
                SingleToneClass.shared.showValidationAlert(target: self, title: "Password Successfully Changed", message: "Login With New Password", OntapOkButton: {
                    
                    
                    self.dismiss(animated:true, completion: {
                        
                        
                    })
                    
                })
                
            }else {
                
                let errorMsg = resultFromServer?.errorMsg!
                print(errorMsg!)
                
                SingleToneClass.shared.showValidationAlert(target: self, title:"Alert!", message:"\(errorMsg!)", OntapOkButton: {
                    
                self.studentIDTF.becomeFirstResponder()
                
            })
            
            
            }
            
        }
        }else {
            
            SingleToneClass.shared.showValidationAlert(target: self, title:"Please Enter Student ID", message: "") {
                
                self.studentIDTF.becomeFirstResponder()
            }
            
            
        }
        
    }
    
    @IBAction func resendOTP(_ sender: Any) {
        
        sendOTPAction((Any).self)
        
        
//        Service.shared.GETService(extraParam:"/student/forgetPassword/\(studentIDTF.text!)") { (responce) -> (Void) in
//
//            print(responce)
//
//
//        }
      
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.dismiss(animated:true) {
             
        }
    }
 

}
