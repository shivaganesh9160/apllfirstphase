//
//  Profile.swift
//  AutoResized@Apll
//
//  Created by synycs on 10/11/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit
import SVProgressHUD
import SkeletonView


var  EmployeeIDFrom  = String()
var IDNumber = String()

var doorGref = String()
var streetGref = String()
var AreaGref = String()
var cityGref = String()
var DistrictGref = String()
var StateGref = String()

class Profile: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    
    @IBOutlet weak var activityIndicatorImg: UIActivityIndicatorView!
    
    
    
    
    
    @IBOutlet weak var pickerBlueBtn: UIButton!
    @IBOutlet weak var updateDetailsBtn: UIButton!
    @IBOutlet weak var profileViewDrop: UIView!
    @IBOutlet weak var centerAreaLbl: UILabel!
    @IBOutlet weak var centerCityLbl: UILabel!
    @IBOutlet weak var centerStateLbl: UILabel!
    @IBOutlet weak var courseLbl: UILabel!
    @IBOutlet weak var mainNameLbl: UILabel!
    @IBOutlet weak var statelbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: UILabel!
    @IBOutlet weak var DOBLbl: UILabel!
    
    
    @IBOutlet weak var doorNumRef: UILabel!
    @IBOutlet weak var streetNumRef: UILabel!
    @IBOutlet weak var areaRef: UILabel!
    @IBOutlet weak var districtRef: UILabel!
    
    
    @IBOutlet weak var doorNumLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var districtLabel: UILabel!
    
    
    
    
    
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var courseLblRef: UILabel!
    @IBOutlet weak var studentDetailsRef: UILabel!
    @IBOutlet weak var emailRef: UILabel!
    @IBOutlet weak var mobileRef: UILabel!
    @IBOutlet weak var dobRef: UILabel!
    @IBOutlet weak var studentCityRef: UILabel!
    @IBOutlet weak var studentStateRef: UILabel!
    @IBOutlet weak var centerAddressRef: UILabel!
    @IBOutlet weak var centerAreaRef: UILabel!
    @IBOutlet weak var centerCityRef: UILabel!
    @IBOutlet weak var centerStateRef: UILabel!
    var profileImgRef = ProfileImgUploadModel()
    var feedBackRes = FeedBackResponseModel()
    @IBOutlet weak var imagePickerBlueView: UIView!
    
    var profileStr = [String]()
    var profileImgStr = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        activityIndicatorImg.color = .black
        activityIndicatorImg.isHidden = true
        
        self.tabBarController?.tabBar.isHidden = false
        
        
        SingleToneClass.shared.showProgressLoading(title: "Loading Please Wait...")
        
//        profileViewDrop.dropShadow(color:.lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        
        
//        pickerBlueBtn.isHidden = true
        
//        self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2.0
//        profileImageView.layer.borderColor = UIColor.cyan.cgColor
//        self.profileImageView.clipsToBounds = true
        
         profileImageView.layer.cornerRadius =  profileImageView.bounds.size.width / 2.0
//        self.profileImageView.layer.borderColor =  (ColorsForViews.thirdBlue as! CGColor)
        profileImageView.layer.borderColor = UIColor.cyan.cgColor
        self.profileImageView.clipsToBounds = true
        
//        self.profileImageView.showAnimatedSkeleton()
//        self.mainNameLbl.showAnimatedSkeleton()
//        self.courseLbl.showAnimatedSkeleton()
//        self.DOBLbl.showAnimatedSkeleton()
//        self.emailLbl.showAnimatedSkeleton()
//        self.mobileNumLbl.showAnimatedSkeleton()
//        self.cityLbl.showAnimatedSkeleton()
//        self.statelbl.showAnimatedSkeleton()
//        self.centerAreaLbl.showAnimatedSkeleton()
//        self.centerCityLbl.showAnimatedSkeleton()
//        self.centerStateLbl.showAnimatedSkeleton()
        
//        self.courseLblRef.showAnimatedSkeleton()
//        self.studentDetailsRef.isHidden = true
//        self.emailRef.showAnimatedSkeleton()
//        self.mobileRef.showAnimatedSkeleton()
//        self.dobRef.showAnimatedSkeleton()
//        self.studentCityRef.showAnimatedSkeleton()
//        self.studentStateRef.showAnimatedSkeleton()
//
//        self.centerAddressRef.isHidden = true
//        self.centerAreaRef.showAnimatedSkeleton()
//        self.centerCityRef.showAnimatedSkeleton()
//        self.centerStateRef.showAnimatedSkeleton()
//
//        self.updateDetailsBtn.isHidden = true
        
        getProfile()
        profileImage()
        //     SVProgressHUD.show(withStatus: "Loading Please Wait")
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        var profileRef =  ProfileImg()
        
        
        profileRef = self.storyboard?.instantiateViewController(withIdentifier: "profileImg") as! ProfileImg
        
        profileRef.modalTransitionStyle = .coverVertical
        
        self.present(profileRef, animated: true) {
            
            profileRef.profileLargeImg.image = self.profileImageView.image
            
            
        }
        
        
    }
    
    
    
    
    
    @IBAction func updateDetailsBtn(_ sender: Any) {
        
        var updateDetailsPage = mainPageForUpdateDetails()
        
        updateDetailsPage = self.storyboard?.instantiateViewController(withIdentifier: "updateVC") as! mainPageForUpdateDetails
        
        
        self.present(updateDetailsPage, animated: true) {
            
            
            
            
            
        }
        
        
    }
    
    
    
    
    
    
    @IBAction func pickerViewBtn(_ sender: Any) {
        
        
        let picker = UIImagePickerController()
        
        picker.delegate = self
        
        picker.sourceType = .photoLibrary
        
        
        present(picker, animated: true, completion: nil)
        
        print("Hello")
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        self.profileImageView.image = tempImage
        self.dismiss(animated:true) {
        uploadImage()
        }
        
        func uploadImage() {
            activityIndicatorImg.startAnimating()
            profileImageView.alpha = 0.5
            activityIndicatorImg.isHidden = false
            let imageData:NSData = profileImageView.image!.jpeg(.low)! as NSData
//            print(imageData)
            let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            self.profileImgRef.studentProfilePic =  imageStr
            self.profileImgRef.studentIdentityNumber = IDNumber
            activityIndicatorImg.isHidden = false
            activityIndicatorImg.startAnimating()
            self.profileImageView.alpha = 1
            DispatchQueue.global(qos: .background).async {
                NetWorkingManager.profileModelFunc(SignUpRequest:  self.profileImgRef) { (result: Any, success: Bool) in
                    if let resultFrom = result as? FeedBackResponseModel {
                        
                        let boolResult = resultFrom.response
                        
                        print(resultFrom)
                        if  boolResult?.count != 0 {
                            DispatchQueue.main.async {
                                self.activityIndicatorImg.stopAnimating()
                                self.activityIndicatorImg.isHidden = true
                                self.profileImageView.alpha = 1
                                SingleToneClass.shared.showValidationAlert(target:self, title:  "Successfully Updated.", message:  " ", OntapOkButton: {
                                })
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.activityIndicatorImg.stopAnimating()
                                self.activityIndicatorImg.isHidden = true
                                SingleToneClass.shared.showValidationAlert(target:self, title:  "Failed To Update Image", message:  "Try Again", OntapOkButton: {
                                })
                            }
                        }
                        print(resultFrom)
                    }
                }
            }
        }
        
        
        
        
        
        
       
//
//        print(imageStr)
        
        
        
//        DispatchQueue.global(qos: .background).async {
//            print("This is run on the background queue")
//            NetWorkingManager.profileModelFunc(SignUpRequest:  self.profileImgRef) { (result: Any, success: Bool) in
//                if let resultFrom = result as? FeedBackResponseModel {
//                    if resultFrom.error == false {
//                        SingleToneClass.shared.showValidationAlert(target:self, title:  "Successfully Updated.", message:  " ", OntapOkButton: {
//                        })
//                    }else{
//                        SingleToneClass.shared.showValidationAlert(target:self, title:  "Failed To Update Image", message:  "Try Again", OntapOkButton: {
//
//
//                        })
//
//                    }
//
//                    print(resultFrom)
//
//                }
//            }
        
       
            
//
//            DispatchQueue.main.async {
//
//                self.activityIndicatorImg.startAnimating()
//
//
//                print("This is run on the main queue, after the previous code in outer block")
//            }
//        }
//
//
     
    }
     
    @IBAction func editButton(_ sender: Any) {
    }
    
     
    func getProfile() {
        //
        Service.shared.GETService(extraParam: "/student/\(EmployeeIDFrom)") { (response) -> (Void) in
            //            print(response)
            if let serverResponse = response as?  NSDictionary {
                let   profileData =  serverResponse["response"] as!    NSDictionary
                //                print(profileData)
                //   let  eventsRes =  profileData[prof]  as! NSDictionary
                let studentName = profileData["studentName"] as!  String
                print(studentName)
                //  let fullName  =  profileData ["studentFullName"] as! String
                let DOB =  profileData ["dateOfBirth"] as! String
                let mobileNum =  profileData ["mobileNumber"] as! String
                let email =  profileData ["emailId"] as! String
                
                
//                if let profileImg = profileData["studentProfilePic"] as? String    {
//
//                    DispatchQueue.main.async {
                
//                        self.profileStr = [profileImg]
                        
                        
//                        print("NO Image")
//                    }
//
//                }else{
//
//                    print(Error.self)
//                }
                
                let course =   profileData ["courseDto"] as! NSDictionary
                let courseDT = course["courseName"] as! String
                if  let studentAddressDto = profileData["studentAddressDto"] as? NSDictionary {
                if let city = studentAddressDto["city"] as? String {
                     DispatchQueue.main.async {
                        self.cityLbl.text = ":  \(city)"
                        
                        cityGref = city
                    }
                }else{
                    
                  print(Error.self)
                    
                }
                if let state =  studentAddressDto["state"] as? String {
                    
                    DispatchQueue.main.async {
                        
                    
                        self.statelbl.text = ":  \(state)"
                         StateGref = state
                    }
                }else{
                    
                    print(Error.self)
                    
                }
                
                if let doorNo =  studentAddressDto["doorNo"] as? String {
                    print("dooooooooo\(doorNo)")
                    DispatchQueue.main.async {
                        
                        self.doorNumLabel.text = ":  \(doorNo)"
                        doorGref = doorNo
                        print("dooooooooo\(doorNo)")
                    }
                    
                }else{
                    
                    print(Error.self)
                    
                }
                
                if let  street =  studentAddressDto["streetName"] as? String {
                    
                    DispatchQueue.main.async {
                        
                        self.streetLabel.text = ":  \(street)"
                        StateGref = street
                        
                    }
                    
                }else{
                    
                    print(Error.self)
                    
                }
                
                
                if let  area =  studentAddressDto["area"] as? String {
                    print("dooooooooo\(area)")
                    DispatchQueue.main.async {
                        
                        self.areaLabel.text = ":  \(area)"
                        
                         AreaGref = area
                        
                    }
                    
                }else{
                    
                    print(Error.self)
                    
                }
                
                
                if let  district =  studentAddressDto["district"] as? String {
                    print("dooooooooo\(district)")
                    DispatchQueue.main.async {
                        
                        self.districtLabel.text = ":  \(district)"
                          DistrictGref = district
                        
                    }
                    
                }else{
                    
                    print(Error.self)
                    
                }
                
                }else{
                    
                    
                    print(Error.self)
                    
                }
                
                if  let centerAddressDto = profileData["centerAddressDto"] as?  NSDictionary {

                if  let centerArea = centerAddressDto["area"] as? String{

                    DispatchQueue.main.async {

                        self.centerAreaLbl.text = ":  \(centerArea)"

                    }

                }else{


                    print(Error.self)

                }
                if let centerCity = centerAddressDto["city"] as? String {

                    DispatchQueue.main.async {

                        self.centerCityLbl.text = ":  \(centerCity)"
                    }

                }else{

                    print(Error.self)

                }


                if let centerState = centerAddressDto["state"] as? String {

                    DispatchQueue.main.async {


                        self.centerStateLbl.text = ":  \(centerState)"

                    }

                }else{


                   print(Error.self)

                }
                }else{
                    
                    
                    print(Error.self)
                    
                    
                }
                
//                for i in 0..<self.profileStr.count{
//
//                    let imgStr = self.profileStr[i]
//
//                    if let decodedData = Data(base64Encoded:  imgStr , options: .ignoreUnknownCharacters) {
//                        let  eventMainImg = UIImage(data: decodedData)
//
//                        self.profileImgStr = eventMainImg!
//
//                    }
//
//                }
                
                DispatchQueue.main.async(execute: {
                    
                    //                    self.fullNameLbl.text = fullName
                    self.mainNameLbl.text = "\(studentName)"
                    
                    print("Test\(studentName)")
                    self.DOBLbl.text = ":  \(DOB)"
                    self.mobileNumLbl.text = ":  \(mobileNum)"
                    self.emailLbl.text = ":  \(email)"
                    self.courseLbl.text = ":  \(courseDT)"
//                    self.cityLbl.text  = city
//                    self.statelbl.text = state
                    //                    self.centerAreaLbl.text = centerArea
                    //                    self.centerCityLbl.text = centerCity
                    //                    self.centerStateLbl.text = centerState
                    //
//                    if self.profileStr.count != 0 {
//
//                        self.profileImageView.image = self.profileImgStr
//
//                    }else{
//
//                        self.profileImageView.image = UIImage(named: "logoDummy")
//
//                    }
                    
                    
                    if  self.mainNameLbl.text!.count != 0 {
                        
                        
                        SingleToneClass.shared.dismissProgressLoading()
                        
//                        self.profileImageView.hideSkeleton()
//                        self.mainNameLbl.hideSkeleton()
//                        self.courseLbl.hideSkeleton ()
//                        self.DOBLbl.hideSkeleton ()
//                        self.emailLbl.hideSkeleton ()
//                        self.mobileNumLbl.hideSkeleton ()
//                        self.cityLbl.hideSkeleton()
//                        self.statelbl.hideSkeleton()
//                        self.centerAreaLbl.hideSkeleton()
//                        self.centerCityLbl.hideSkeleton()
//                        self.centerStateLbl.hideSkeleton()
//
//                        self.studentDetailsRef.isHidden = false
//                        self.courseLblRef.hideSkeleton()
//                        self.studentDetailsRef.hideSkeleton()
//                        self.emailRef.hideSkeleton()
//                        self.mobileRef.hideSkeleton()
//                        self.dobRef.hideSkeleton()
//                        self.studentCityRef.hideSkeleton()
//                        self.studentStateRef.hideSkeleton()
//
//                        self.centerAddressRef.isHidden = false
//                        self.centerAreaRef.hideSkeleton()
//                        self.centerCityRef.hideSkeleton()
//                        self.centerStateRef.hideSkeleton()
//                        self.updateDetailsBtn.isHidden = false
//
//                        self.pickerBlueBtn.isHidden = false
//
                        
                    }
                    
                })
                
            }
        }
        
    }
    
    
    
    func profileImage(){
        
        activityIndicatorImg.startAnimating()
        Service.shared.GETService(extraParam: "/student/getStudentProfilePic/\(EmployeeIDFrom)") { (response) -> (Void) in
            
            
            if let serverResponse = response as?  NSDictionary {
                let profileData =  serverResponse["response"] as!    NSDictionary
            
                if  let studentImg = profileData["studentProfilePic"] as? String {
//                    print(studentImg)
                    DispatchQueue.main.async {
                        self.profileStr = [studentImg]
                        for i in 0..<self.profileStr.count{
                            let imgStr = self.profileStr[i]
                            if let decodedData = Data(base64Encoded:  imgStr , options: .ignoreUnknownCharacters) {
                                let  eventMainImg = UIImage(data: decodedData)
                                self.profileImgStr = eventMainImg!
                            }
                        }
                        self.profileImageView.image = self.profileImgStr
                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
                        self.profileImageView.isUserInteractionEnabled = true
                        self.profileImageView.addGestureRecognizer(tapGestureRecognizer)
                    }
                }else{
                     self.profileImageView.image = UIImage(named: "logoDummy")
                     print("No Image")
                      print(Error.self)
                }
                 DispatchQueue.main.async {
                if self.profileStr.count != 0 {
                    self.activityIndicatorImg.stopAnimating()
                }else{
                
                }
                
                }
             
        }
       
    }
    
    }
    
}


extension UIView {
    
    // OUTPUT 1
    //    func dropShadow(scale: Bool = true) {
    //        layer.masksToBounds = false
    //        layer.shadowColor = UIColor.black.cgColor
    //        layer.shadowOpacity = 0.5
    //        layer.shadowOffset = CGSize(width: -1, height: 1)
    //        layer.shadowRadius = 1
    //
    //        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    //        layer.shouldRasterize = true
    //        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    //    }
    //
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
        
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

