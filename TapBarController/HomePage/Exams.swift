//
//  Exams.swift
//  AutoResized@Apll
//
//  Created by synycs on 15/11/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit

class Exams: UIViewController {
  
    @IBOutlet weak var certificatesView: UIView!
    @IBAction func certificateBtn(_ sender: Any) {
        
        certificatesView.isHidden = false
        
        }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        
        dismiss(animated: true) {
            
        }
    }
    
    
override func viewDidLoad() {
        super.viewDidLoad()

        certificatesView.isHidden = true
        
}
  
}
