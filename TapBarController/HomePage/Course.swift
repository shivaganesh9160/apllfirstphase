//
//  Course.swift
//  TapBarController
//
//  Created by synycs on 29/10/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit
import SkeletonView
class Course: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var unitNamesLbl: UILabel!
    
    
    
    
    
    var myVariable = 0
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if unitsFromServer.count == 0{
            
            return 2
            
        }else{
        
        return self.unitsFromServer.count
            
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "courseCell") as! CourseCell
         
        if unitsFromServer.count != 0 {
            
            
        cell.numberOfUnits.text = self.unitsFromServer[indexPath.row]
            
//        cell.unitNumberLabel.text = String(myVariable)
            
        }
        return cell
    }
    
    var progressBarTimer:Timer!
    var courseNames = [String]()
    
    @IBOutlet weak var tableViewForUnits: UITableView!
    
    
     
    
    @IBOutlet var lineOut: UIView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    
    @IBOutlet weak var courseDetailsLbl: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var scheduleBtn: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var curriculamAction: UIButton!
    
    
    var unitsFromServer = [String]()
    
    @IBAction func backButton(_ sender: UIButton) {
        
        dismiss(animated:true) {
           
        }
    }
    
    
    @IBAction func colorBtn1(_ sender: Any) {
        
        scheduleBtn.backgroundColor = ColorsForViews.thirdBlue
        scheduleBtn.setTitleColor(UIColor.white, for:.normal)
        curriculamAction.backgroundColor = UIColor.white
        curriculamAction.setTitleColor(UIColor.black, for:.normal)
    }
    
    
    @IBAction func curriculamTD(_ sender: Any) {
        
        scheduleBtn.backgroundColor = UIColor.white
        scheduleBtn.setTitleColor(UIColor.black, for:.normal)
        scheduleBtn.backgroundColor = UIColor.white
        
        curriculamAction.backgroundColor = ColorsForViews.thirdBlue
        curriculamAction.setTitleColor(UIColor.white, for:.normal)
    }

    
    @IBAction func curriAct(_ sender: Any) {
        
        
        SingleToneClass.shared.showProgressLoading(title: "Units Loading...")
        
        
        DispatchQueue.main.async {
            
          
            if self.unitsFromServer.count != 0 {
        
                
                SingleToneClass.shared.dismissProgressLoading()
                
                self.tableViewForUnits.isHidden = false
                self.unitNamesLbl.isHidden = false
            }
        
        }
        
        
        
        //        self.label1.text = "Curriculam Details Here"
        //        self.label2.text = "Curriculam Details Here"
        //        self.label3.text = "Curriculam Details Here"
        //        self.label4.text = "Curriculam Details Here"
        //        self.label5.text = "Curriculam Details Here"
        
    }
    
    @IBAction func sheduleAction(_ sender: Any) {
        //
        //        self.label1.text = "Schedule Details Here"
        //        self.label2.text = "Schedule Details Here"
        //        self.label3.text = "Schedule Details Here"
        //        self.label4.text = "Schedule Details Here"
        //        self.label5.text = "Schedule Details Here"
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        unitNamesLbl.isHidden = true
        
        
        courseFromServer()
        
        self.myVariable += 1
        tableViewForUnits.isHidden = true
        scheduleBtn.isHidden = true
        scheduleBtn.backgroundColor = ColorsForViews.thirdBlue
        scheduleBtn.setTitleColor(UIColor.white, for:.normal)
        //        self.label1.text = "Schedule Details Here"
        //        self.label2.text = "Schedule Details Here"
        //        self.label3.text = "Schedule Details Here"
        //        self.label4.text = "Schedule Details Here"
        //        self.label5.text = "Schedule Details Here"
//        progressView.progress = 0.0
//        self.progressBarTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(Course.updateProgressView), userInfo: nil, repeats: true)
//        
//        progressView.progressTintColor = ColorsForViews.thirdBlue
//        progressView.progressViewStyle = .default
//        progressView.subviews[1].clipsToBounds = true
//        progressView.clipsToBounds = true
//        
//        self.label1.showAnimatedSkeleton()
//        self.label2.showAnimatedSkeleton()
//        self.label3.showAnimatedSkeleton()
//        self.label4.showAnimatedSkeleton()
//        self.label5.showAnimatedSkeleton()
//        self.courseDetailsLbl.showAnimatedSkeleton()
//        
//     self.view1.layer.cornerRadius = self.view1.bounds.size.width / 2.0
//     self.view2.layer.cornerRadius = self.view2.bounds.size.width / 2.0
//     self.view3.layer.cornerRadius = self.view3.bounds.size.width / 2.0
//     self.view4.layer.cornerRadius = self.view4.bounds.size.width / 2.0
//     self.view5.layer.cornerRadius = self.view5.bounds.size.width / 2.0
//    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func updateProgressView(){
        progressView.progress += 0.1
        progressView.setProgress(progressView.progress, animated: true)
        if(progressView.progress == 1.0)
        {
            progressBarTimer.invalidate()
        }
    }
    
    func courseFromServer(){
        
        Service.shared.GETService(extraParam: "/student/\(EmployeeIDFrom)") { (response) -> (Void) in
            
            print(response)
            
            if let serverResponse = response as?  NSDictionary {
                
                let response =  serverResponse["response"] as!    NSDictionary
                let eventName =  response["courseDto"] as!  NSDictionary
                let courseName = eventName["courseName"] as! String
                
                
//                let courseDur = eventName["courseDuration"] as!  Int16
                
                
                let curriculumDto = eventName["curriculumDto"] as! NSDictionary
         
                
                let unitNames = curriculumDto["listOfUnits"] as! NSArray
                
                for i in 0 ..< unitNames.count {
                    
                    
                    let totalNames = unitNames[i] as! NSDictionary
                    
                    let names = totalNames["name"] as! String
                    
                    DispatchQueue.main.async {
                        
                    
                        
                       self.unitsFromServer.append(names)
                        print("UNitssssss\(self.unitsFromServer)")
                        self.tableViewForUnits.reloadData()
                  
                    
                    
                    }
                    
                    
                    
                    
                    
                }
                
                
               
                
                
                
               
//                self.unitsFromServer.append(units!)
                
                
                print(self.unitsFromServer)
                
                
                    print(eventName)
                DispatchQueue.main.async(execute: {
                        
                    self.courseDetailsLbl.text = courseName
                    
                    
                    
//                        self.label1.text = "\(courseDur)"
                    
//                    if self.courseDetailsLbl.text?.count != 0 {
//
//                        self.label1.hideSkeleton()
//                        self.label2.hideSkeleton()
//                        self.label3.hideSkeleton()
//                        self.label4.hideSkeleton()
//                        self.label5.hideSkeleton()
//                        self.courseDetailsLbl.hideSkeleton()
//
//
//                    }
                    
                    })
                
            }
        
    }
    
     
        }
    
    
        }
    



