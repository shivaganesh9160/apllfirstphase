//
//  HomeScreen.swift
//  TapBarController
//
//  Created by synycs on 25/10/18.
//  Copyright © 2018 synycs. All rights reserved.
//

var eventsNames = [String]()
var eventDates = [String]()
var eventsDescription = [String]()
var imageString = [String]()
var locationStr = [String]()
var LocalDate = String()
var initialDesc = [String]()
var bannerData:EventsModel?

import UIKit
import SVProgressHUD

class  HomePage: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    var x: Int = 0
    var strIndex:Int = 1
    
    @IBOutlet weak var popUpBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var courseCollection: UICollectionView!
    @IBOutlet weak var bannerCollection: UICollectionView!
    @IBOutlet weak var popOutBtn: UIButton!
    @IBAction func OutBtn(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                exit(0)
            }
        }
    }
    
    
    @IBOutlet weak var homeGradientView: GradientView!
    
    
    var images = ["books (1)","test","calendar","approve",]
    var names = ["Program","Exams","Events","Feedback",]
    
    
    @IBAction func logOutBtn(_ sender: UIButton) {
        popUpBtn.isHidden = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        
        pageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bannerCollection{
            
            
            if   eventsNames.count != 0 {
                
                
                pageControl.numberOfPages =  eventsNames.prefix(5).count
                
                
                return  eventsNames.prefix(5).count
                
            }else{
                
                pageControl.numberOfPages = 4
                
                return 4
            }
            
        }else{
            
            
            return 4
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == bannerCollection{
            
            let cell = bannerCollection.dequeueReusableCell(withReuseIdentifier: "abc", for: indexPath) as! BannerCVCell
            
            
            if  eventsNames.count != 0 {
                
                cell.hideAnimation()
                
                let  eventsNames5 =  eventsNames.prefix(5)
                
                print(eventsNames5)
                
                let  image5 =  imageString.prefix(5)
                
                let  location5 =  locationStr.prefix(5)
                
                let  date5 =  eventDates.prefix(5)
                
                
                let dataDecoded : Data = Data(base64Encoded: image5[indexPath.row], options: .ignoreUnknownCharacters)!
                
                let decodedimage = UIImage(data: dataDecoded)
                
                cell.bannerImg.image = decodedimage
                
                
                cell.bannerTitle.text =  eventsNames5[indexPath.row]
                
                cell.bannerLocation.text = location5[indexPath.row]
                
                //  cell.bannerDescription.text = description5[indexPath.row]
                
                let does =  date5[indexPath.row]
                
                let myDate = does
                
                let dateFormatter = DateFormatter()
                
                dateFormatter.dateFormat = "yyyy-MM-dd"
                
                
                let date = dateFormatter.date(from:myDate)!
                
                
                dateFormatter.dateFormat = "d MMM\nyyyy"
                
                let dateString = dateFormatter.string(from:date)
                
                
                cell.bannerDate.text =  dateString
                
                
                
            }
            
            return cell
            
        }else{
            
            let cell =  courseCollection.dequeueReusableCell(withReuseIdentifier: "123", for: indexPath)as! CollectionViewCell
            cell.imgeView.image = UIImage(named:self.images[indexPath.row])
            cell.label.text = names[indexPath.row]
            
            return cell
            
            
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == courseCollection {
            
            
            let imgWidth = (collectionView.bounds.width / 2)
            return CGSize(width: imgWidth, height: collectionView.bounds.height/2)
            
            
        }else if collectionView == bannerCollection {
            
            return CGSize(width:bannerCollection.frame.width, height: bannerCollection.frame.height)
            
        }else{
            
            return CGSize(width:bannerCollection.frame.width, height: bannerCollection.frame.height)
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        var visibleRect = CGRect()
        
        visibleRect.origin = bannerCollection.contentOffset
        visibleRect.size =  bannerCollection.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath =  bannerCollection.indexPathForItem(at: visiblePoint) else { return }
        
        x = indexPath.row
        strIndex = indexPath.row
        
        let indexPathrow = IndexPath(item: x, section: 0)
        self.bannerCollection.scrollToItem(at: indexPathrow, at: .centeredHorizontally, animated: true)
        
        
        
        print(indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView ==  courseCollection{
            
            if indexPath.row == 0{
                
                var courseRef = Course()
                
                courseRef = self.storyboard?.instantiateViewController(withIdentifier:"course") as! Course
                self.present(courseRef, animated: true) {
                    
                }
                
                
                
               
            }else  if indexPath.row == 2 {
                
                var attendanceRef =  ApllEvents()
                
                
                attendanceRef = self.storyboard?.instantiateViewController(withIdentifier:"eventsVC") as! ApllEvents
                
                self.present(attendanceRef, animated:  true) {
                    
                }
              
                print(indexPath.row)
                
                
            }else if indexPath.row == 1{
                
                var  examRef =   Exams ()
                
                examRef = self.storyboard?.instantiateViewController(withIdentifier: "exams") as!    Exams
                
                self.present(examRef, animated: true) {
                    
                }
                
            }else if indexPath.row == 3 {
                
                var feedRef =  Rating ()
                
                feedRef = self.storyboard?.instantiateViewController(withIdentifier: "Rating") as!   Rating
                
                self.present(feedRef, animated: true) {
                    
                }
                
                
            }
        }else{
            
        }
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        popUpBtn.isHidden = true
        
        //        getHomePageEvents()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
        self.homeGradientView.isUserInteractionEnabled = true
        self.homeGradientView.addGestureRecognizer(tapGestureRecognizer)
        
        if eventDates.count == 0 {
            
            eventsGetModel ()
            
        }else{
            
            
            print(Error.self)
        }
        
        let floawLayout = UPCarouselFlowLayout()
        floawLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 50 , height: bannerCollection.frame.size.height)
        floawLayout.scrollDirection = .horizontal
        floawLayout.sideItemScale = 0.8
        floawLayout.sideItemAlpha = 1.0
        floawLayout.spacingMode = .fixed(spacing: 10.0)
        bannerCollection.collectionViewLayout = floawLayout
        bannerCollection.isPagingEnabled = true
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        self.popUpBtn.isHidden = true
        
    }
    
    
    
    override func  viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func eventsGetModel (){
         
        NetWorkingManager.eventsModelFunc { (result: Any, success: Bool) in
            
            if let response  = result as? EventsModel {
                bannerData = response
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let dateInFormat = dateFormatter.string(from: NSDate() as Date)
                
                 LocalDate.append(dateInFormat)
                
                print("CurrentDate\(dateInFormat)")
                let filteredArray = response.response.filter({($0.eventDate >=  dateInFormat )})
                
                
                filteredArray.map({ (responseSt)   in
                    
                     
                        
                        eventDates.append(responseSt.eventDate)
                        print("SortedDates\(eventDates)")
                        eventsNames.append(responseSt.eventname)
                        print("EventsFromSort\(eventsNames)")
                        imageString .append(responseSt.image)
                        locationStr .append(responseSt.location)
                        
                        eventsDescription.append(responseSt.eventDescription)
                        
                        let first50 = responseSt.eventDescription.prefix(80)
                        initialDesc.append(String(first50))
                    
                    DispatchQueue.main.async {
                        self.bannerCollection.reloadData()
                        
                    }
                    
                    
                    
                })
                
                
                //                bannerData?.response.map({ (EventsModel)  in
                //
                //                    eventDates.append(EventsModel.eventDate)
                //                    eventsNames.append(EventsModel.eventname)
                //                    imageString.append(EventsModel.image)
                //                    locationStr.append(EventsModel.location)
                //                    self.bannerCollection.reloadData()
                //                    print(eventDates.sorted())
                //
                //                })
                //                bannerData?.response.forEach({ (responseSt) in
                //                    responseSt.
                //                })
                //
                //
            } else {
                print("error in getting data from resonse")
            }
        }
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
  
    
    
    
    
    
    
    
    
    func getHomePageEvents() {
        
        Service.shared.GETService(extraParam: "/event") { (response) -> (Void) in
            
            //            print(response)
            
            if let serverResponse = response as?  NSDictionary {
                
                let  response =  serverResponse["response"] as!   NSArray
                
                
                
                for i in 0..<response.count{
                    
                    let eventsRes = response[i] as! NSDictionary
                    let eventName =  eventsRes["eventname"] as! String
                    let Dates =   eventsRes["eventDate"] as! String
                    let description  =  eventsRes["description"] as! String
                    let imageFormat = eventsRes["image"] as! String
                    let location = eventsRes["location"] as! String
                    
                    
                    let first50 = description.prefix(80)
                    
                    initialDesc.append(String(first50))
                    
                    eventsNames.append(eventName)
                    print(eventName)
                    eventDates.append(Dates)
                    eventsDescription.append(description)
                    imageString.append(imageFormat)
                    locationStr.append(location)
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.bannerCollection.reloadData()
                        
                        // SingleToneClass.shared.dismissProgressLoading()
                        
                    })
                    
                }
                
            }
            
        }
        
    }
    
    
}


