//
//  ViewController.swift
//  EventsTest
//
//  Created by SujiRaj on 15/11/18.
//  Copyright © 2018 MPME. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var eventTableView: UITableView!
    var selectedIndex: Int = -1
    
    var expandText = "lets you create anything from simple paths to complex polygons by adding a set of lines, arcs, curves and rendering it into a custom view. So first define the path of the progress bar you need using this. For our example, I have created a circular path"
    var collapseText = "lets you create anything from simple paths to complex polygons by adding a set of lines, arcs, curves and rendering it into a custom view."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventTableView.estimatedRowHeight = 150
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "CollapseCell", bundle: nil), forCellReuseIdentifier: "CollapseCell")
        if let cell: CollapseCell = tableView.dequeueReusableCell(withIdentifier: "CollapseCell", for: indexPath) as? CollapseCell {
            cell.downArrowButton.layer.cornerRadius = cell.downArrowButton.frame.width / 2
            cell.downArrowButton.tag = indexPath.row
            if self.selectedIndex == indexPath.row {
                let hei = heightForView(text: self.expandText, font: UIFont(name: "Helvetica Neue", size: 18)!, width: cell.descTextLabel.frame.width)
                
                cell.contentViewConstraint.constant = hei + 45
                cell.descTextLabel.text = self.expandText
                cell.downArrowButton.setImage(UIImage(named: "upArrow"), for: .normal)
            }else {
                let hei = heightForView(text: self.collapseText, font: UIFont(name: "Helvetica Neue", size: 18)!, width: cell.descTextLabel.frame.width)
                cell.contentViewConstraint.constant = hei + 45
                cell.descTextLabel.text = self.collapseText
                cell.downArrowButton.setImage(UIImage(named: "downArrow"), for: .normal)
            }
            
            cell.downArrowButton.addTarget(self, action: #selector(ViewController.downArrowTapped(_:)), for: .touchUpInside)
            
            
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func downArrowTapped(_ sender: UIButton) {
        if self.selectedIndex != sender.tag {
            self.selectedIndex = sender.tag
        }else{
            self.selectedIndex = -1
        }
        self.eventTableView.reloadData()
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    

}

