//
//  CollapseCell.swift
//  EventsTest
//
//  Created by SujiRaj on 15/11/18.
//  Copyright © 2018 MPME. All rights reserved.
//

import UIKit

class CollapseCellEvents: UITableViewCell {

    @IBOutlet weak var downArrowButton: UIButton!
    @IBOutlet weak var contentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var descTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
