//
//  Constants.swift
 

import Foundation
import UIKit

struct ColorsForViews {
    
    static let firstBlue = UIColor(red: 33.0/255.0, green: 50.0/255.0, blue: 217.0/255.0, alpha: 1.0)
    static let  secondBlue = UIColor(red: 50.0/255.0, green: 87.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let  thirdBlue  = UIColor(red: 0.0/255.0, green: 146.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    static let  shimmerColor = UIColor(red:  214/255.0, green: 214/255.0, blue: 214/255.0, alpha: 1.0)
    static let lightGrey = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha:1.0)
    static let black = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let white = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
}
