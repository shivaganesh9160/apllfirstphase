//
//  Rating.swift
//  AutoResized@Apll
//
//  Created by synycs on 14/11/18.
//  Copyright © 2018 synycs. All rights reserved.
//


var enrollmentDate = String()


import UIKit
import Alamofire
import EVReflection

class Rating: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var feedbackTableView: UITableView!
    
    @IBOutlet weak var feedBackOut: UIButton!
    
    @IBAction func backButton(_ sender: UIButton) {
        
        dismiss(animated: true) {
            
        }
    }
    var feedbackRequest = FeedBackModel()
    var details = studentFeedBackDetails()
    var feedback = studentFeedbackDetailsD()
    var userDict: [String: Any] = [String : Any]()
    var userDefaultKey: String = "feedbacks"
    var userDefaultKey2: String = "feedbacks2"
    var feedbackSelectedIndex: Int = -1
    var feedbackOptionSelectedIndex: Int = -1
    var indexPathRow: Int = Int()
    
    
    
    let afterCourse = ["Exams conducted on time?", "Certificates?", "How was your experience being a part of APLL?", "Whom will you refer this program?"]
    
    let beforeEnrollment = ["Location of the Center?","Were you able to locate the center?","Center Co-ordination?","Center Infra?"]
    
    let afterEnrollment = ["Classes are starting on time?","Feedback of Faculty?","Availability of Computers in practical classes?","How is the Program?"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        details.id = "String"
        feedbackRequest.studentFeedbackId = details
        feedback.studentFeedBackDetailsId.id = IDNumber
        feedbackRequest.studentId = EmployeeIDFrom
        self.feedbackTableView.estimatedRowHeight = 60
        feedbackRequest.studentFeedbackDetailsDto.removeAll()
        
        
        if enrollmentDate.count == 0 {
        
        feedbackRequest.feedbackType = "INITAIL_FEEDBACK"
            
        }else if enrollmentDate >= LocalDate{
             
                 feedbackRequest.feedbackType = "MIDDLE_FEEDBACK"
            
        }else if enrollmentDate >= LocalDate{
            
            
             feedbackRequest.feedbackType = "LAST_FEEDBACK"
            
            
        }
//           enrollmentDate = "2019-04-22"
        
        print(enrollmentDate)
        
        
        if enrollmentDate >= LocalDate {
            
            print("homeEnrollMent\(enrollmentDate)")
            print("homeLocalDate\(LocalDate)")
            print("DuringInCourse")
            
        }else{
            print("homeEnrollMent\(enrollmentDate)")
            print("homeLocalDate\(LocalDate)")
            
            print("AfterCourse")
            
            
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.afterCourse.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.feedbackSelectedIndex == indexPath.row {
            tableView.register(UINib(nibName: "ExpandCell", bundle: nil), forCellReuseIdentifier: "ExpandCell")
            if let cell: ExpandCell = tableView.dequeueReusableCell(withIdentifier: "ExpandCell", for: indexPath) as? ExpandCell {
                
                
                if enrollmentDate.count == 0 {
                    
                    cell.feedbackTextLabel.text = beforeEnrollment[indexPath.row]
                    self.indexPathRow = indexPath.row
                    cell.feedbackHeaderButton.tag = indexPath.row
                    
                   
                    
                    if let userData: [String: Any] = UserDefaults.standard.dictionary(forKey: self.userDefaultKey) {
                        
                        
                        // print(userData)
                        if userData.keys.contains(cell.feedbackTextLabel.text!) {
                            let val: Int = userData[cell.feedbackTextLabel.text!] as! Int
                            switch val {
                            case 1001:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "selected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            case 1002:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "selected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            case 1003:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "selected")
                            default:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            }
                        }else{
                            cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                            cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                            cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                        }
                    }else{
                        cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                        cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                        cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    }
                    
                    cell.excellentViewButton.addTarget(self, action: #selector( Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.goodViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.badViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.feedbackHeaderButton.addTarget(self, action: #selector(Rating.feedbackTextButtonTapped(_:)), for: .touchUpInside)
                    
                    
                    
                    
                    
                    if indexPath.row == 0 {
                        
                        feedback.question = self.beforeEnrollment[0]
                        
                        if enrollmentDate.count == 0 {
                            
                            cell.feedBack1.text =  "Near"
                            cell.feedBack2.text =  "Very Near"
                        }else if enrollmentDate.count != 0 {
                            
                            cell.feedBack1.text = "Yes"
                            cell.feedBack2.text =  "No"
                            
                        }else if enrollmentDate >= LocalDate {
                            
                            cell.feedBack1.text = "Yes"
                            cell.feedBack2.text = "Delayed"
                            
                            
                            
                            
                        }
                        
                    }else if indexPath.row == 1 {
                        
                        feedback.question = self.beforeEnrollment[1]
                        
                        if enrollmentDate.count == 0 {
                            
                            
                            cell.feedBack1.text = "Very Easily"
                            cell.feedBack2.text =  "Easily"
                            
                        }else{
                            
                            cell.feedBack1.text = "Very Good"
                            cell.feedBack2.text =  "Good"
                            
                            
                        }
                    } else if indexPath.row == 2 {
                        feedback.question = self.beforeEnrollment[2]
                        
                        if enrollmentDate.count == 0 {
                            
                            cell.feedBack1.text =  "Very Supportive"
                            cell.feedBack2.text =  "Supportive"
                        }else{
                            
                            cell.feedBack1.text =  "Single PC Available"
                            cell.feedBack2.text =  "Shared PC Provided"
                            
                        }
                        
                    } else if indexPath.row == 3 {
                        
                        feedback.question = self.beforeEnrollment[3]
                        
                        if enrollmentDate.count == 0 {
                            
                            cell.feedBack1.text =  "Excellent"
                            cell.feedBack2.text =  "Very Good"
                            
                        }else{
                            
                            cell.feedBack1.text =  "Excellent"
                            cell.feedBack2.text =  "Very Good"
                            
                        }
                    }
                    
                    return cell
                    
                }else if enrollmentDate >= LocalDate {
                    
                    cell.feedbackTextLabel.text = afterEnrollment[indexPath.row]
                    self.indexPathRow = indexPath.row
                    cell.feedbackHeaderButton.tag = indexPath.row
                    
                    if let userData: [String: Any] = UserDefaults.standard.dictionary(forKey: self.userDefaultKey) {
                        
                        
                        // print(userData)
                        if userData.keys.contains(cell.feedbackTextLabel.text!) {
                            let val: Int = userData[cell.feedbackTextLabel.text!] as! Int
                            switch val {
                            case 1001:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "selected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            case 1002:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "selected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            case 1003:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "selected")
                            default:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            }
                        }else{
                            cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                            cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                            cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                        }
                    }else{
                        cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                        cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                        cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    }
                    
                    cell.excellentViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.goodViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.badViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.feedbackHeaderButton.addTarget(self, action: #selector(Rating.feedbackTextButtonTapped(_:)), for: .touchUpInside)
                    
                    
                    if indexPath.row == 0 {
                        
                        feedback.question = self.afterEnrollment[0]
                        
                        if enrollmentDate.count == 0 {
                            
                            cell.feedBack1.text =  "Near"
                            cell.feedBack2.text =  "Very Near"
                        }else{
                            
                            cell.feedBack1.text = "Yes"
                            cell.feedBack2.text =  "No"
                            
                        }
                        
                    }else if indexPath.row == 1 {
                        
                        feedback.question = self.afterEnrollment[1]
                        
                        if enrollmentDate.count == 0 {
                            
                            
                            cell.feedBack1.text = "Very Easily"
                            cell.feedBack2.text =  "Easily"
                            
                        }else{
                            
                            cell.feedBack1.text = "Very Good"
                            cell.feedBack2.text =  "Good"
                            
                            
                        }
                    } else if indexPath.row == 2 {
                        feedback.question = self.afterEnrollment[2]
                        
                        if enrollmentDate.count == 0 {
                            
                            cell.feedBack1.text =  "Very Supportive"
                            cell.feedBack2.text =  "Supportive"
                        }else{
                            
                            cell.feedBack1.text =  "Single PC Available"
                            cell.feedBack2.text =  "Shared PC Provided"
                            
                        }
                        
                    } else if indexPath.row == 3 {
                        
                        feedback.question = self.afterEnrollment[3]
                        
                        if enrollmentDate.count == 0 {
                            
                            cell.feedBack1.text =  "Excellent"
                            cell.feedBack2.text =  "Very Good"
                            
                        }else{
                            
                            cell.feedBack1.text =  "Excellent"
                            cell.feedBack2.text =  "Very Good"
                            
                        }
                    }
                    
                    
                    return cell
                    
                    
                    
                }else if enrollmentDate <= LocalDate  {
                    cell.feedbackTextLabel.text = afterCourse[indexPath.row]
                    self.indexPathRow = indexPath.row
                    cell.feedbackHeaderButton.tag = indexPath.row
                    
                    if let userData: [String: Any] = UserDefaults.standard.dictionary(forKey: self.userDefaultKey) {
                        
                        
                        // print(userData)
                        if userData.keys.contains(cell.feedbackTextLabel.text!) {
                            let val: Int = userData[cell.feedbackTextLabel.text!] as! Int
                            switch val {
                            case 1001:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "selected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            case 1002:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "selected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            case 1003:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "selected")
                            default:
                                cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                                cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                            }
                        }else{
                            cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                            cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                            cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                        }
                    }else{
                        cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                        cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                        cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    }
                    
                    cell.excellentViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.goodViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.badViewButton.addTarget(self, action: #selector(Rating.feedbackOptionButtonTapped(_:)), for: .touchUpInside)
                    cell.feedbackHeaderButton.addTarget(self, action: #selector(Rating.feedbackTextButtonTapped(_:)), for: .touchUpInside)
                    
                    
                    
                    
                    if indexPath.row == 0 {
                        
                        feedback.question = self.afterCourse[0]
                        
                        cell.feedBack1.text =  "Yes"
                        cell.feedBack2.text =  "Delayed"
                        
                    }else if indexPath.row == 1 {
                        
                        feedback.question = self.afterCourse[1]
                        cell.feedBack1.text =  "Collected"
                        cell.feedBack2.text =  "Yet to be collected from centre"
                       
                    } else if indexPath.row == 2 {
                        feedback.question = self.afterCourse[2]
                        
                        cell.feedBack1.text =  "Excellent"
                        cell.feedBack2.text =  "Very Good"
                        
                    } else if indexPath.row == 3 {
                        
                        feedback.question = self.afterCourse[3]
                        
                        cell.feedBack1.text =  "Friends"
                        cell.feedBack2.text =  "Family Members"
                    }
                    
                    
                    
                    
                    return cell
                }
                
                //                if indexPath.row == 0 {
                //
                //                    feedback.question = self.feedbackText[0]
                //
                //                    if enrollmentDate.count == 0 {
                //
                //                    cell.feedBack1.text =  "Near"
                //                    cell.feedBack2.text =  "Very Near"
                //                    }else{
                //
                //                        cell.feedBack1.text = "Yes"
                //                        cell.feedBack2.text =  "No"
                //
                //                    }
                //
                //                }else if indexPath.row == 1 {
                //
                //                    feedback.question = self.feedbackText[1]
                //
                //                    if enrollmentDate.count == 0 {
                //
                //
                //                    cell.feedBack1.text = "Very Easily"
                //                    cell.feedBack2.text =  "Easily"
                //
                //                    }else{
                //
                //                        cell.feedBack1.text = "Very Good"
                //                        cell.feedBack2.text =  "Good"
                //
                //
                //                    }
                //                } else if indexPath.row == 2 {
                //                    feedback.question = self.feedbackText[2]
                //
                //                    if enrollmentDate.count == 0 {
                //
                //                    cell.feedBack1.text =  "Very Supportive"
                //                    cell.feedBack2.text =  "Supportive"
                //                    }else{
                //
                //                        cell.feedBack1.text =  "Single PC Available"
                //                        cell.feedBack2.text =  "Shared PC Provided"
                //
                //                    }
                //
                //                } else if indexPath.row == 3 {
                //
                //                    feedback.question = self.feedbackText[3]
                //
                //                    if enrollmentDate.count == 0 {
                //
                //                    cell.feedBack1.text =  "Excellent"
                //                    cell.feedBack2.text =  "Very Good"
                //                    }else{
                //
                //                        cell.feedBack1.text =  "Excellent"
                //                        cell.feedBack2.text =  "Very Good"
                //
                //
                //                    }
                //                }
                //                else {
                //                    feedback.question = self.feedbackText[4]
                //                }
                
                //                return cell
            }
        }else {
            tableView.register(UINib(nibName: "CollapseCell", bundle: nil), forCellReuseIdentifier: "CollapseCell")
            if let cell: CollapseCell = tableView.dequeueReusableCell(withIdentifier: "CollapseCell", for: indexPath) as? CollapseCell {
                
                //                cell.feedbackTextLabel.text =   beforeEnrollment[indexPath.row]
                
                
                if enrollmentDate.count == 0 {
                    
                    cell.feedbackTextLabel.text =   beforeEnrollment[indexPath.row]
                    
                    
                }else if enrollmentDate >= LocalDate{
                    
                    cell.feedbackTextLabel.text =   afterEnrollment[indexPath.row]
                    
                }else if enrollmentDate <= LocalDate{
                    
                    
                    cell.feedbackTextLabel.text = afterCourse[indexPath.row]
                    
                    
                }
                
                
                //                cell.feedbackTextLabel.text = feedbackText[indexPath.row]
                cell.feedbackContentViewButton.tag = indexPath.row
                cell.feedbackContentViewButton.addTarget(self, action: #selector(Rating.feedbackTextButtonTapped(_:)), for: .touchUpInside)
                return cell
            }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func feedbackTextButtonTapped(_ sender: UIButton) {
        
        if self.feedbackSelectedIndex != sender.tag {
            
            self.feedbackSelectedIndex = sender.tag
            
        }else {
            
            self.feedbackSelectedIndex = -1
            
        }
        
        self.feedbackTableView.reloadData()
    }
    
    @objc func feedbackOptionButtonTapped(_ sender: UIButton) {
        if enrollmentDate.count == 0 {
            let idxPath: IndexPath = IndexPath(row: self.indexPathRow, section: 0)
            if let cell: ExpandCell = self.feedbackTableView.cellForRow(at: idxPath) as? ExpandCell {
                
                
                
                let text = self.beforeEnrollment[self.indexPathRow]
                self.userDict[text] = sender.tag
                //Saving the data in locally
                UserDefaults.standard.set(self.userDict as Any, forKey: self.userDefaultKey)
                print(self.userDict)
                
                switch sender.tag {
                case 1001:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "selected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    print(sender.tag)
                    
                    feedback.feedback =  cell.feedBack1.text
                    
                case 1002:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "selected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    print(sender.tag)
                    
                    feedback.feedback =  cell.feedBack2.text
                    
                case 1003:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "selected")
                default:
                    break;
                }
            }
        }else if enrollmentDate >= LocalDate {
            
            let idxPath: IndexPath = IndexPath(row: self.indexPathRow, section: 0)
            if let cell: ExpandCell = self.feedbackTableView.cellForRow(at: idxPath) as? ExpandCell {
                
                
                
                let text = self.afterEnrollment[self.indexPathRow]
                self.userDict[text] = sender.tag
                //Saving the data in locally
                UserDefaults.standard.set(self.userDict as Any, forKey: self.userDefaultKey)
                print(self.userDict)
                
                switch sender.tag {
                case 1001:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "selected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    feedback.feedback =  cell.feedBack1.text
                case 1002:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "selected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    feedback.feedback =  cell.feedBack2.text
                case 1003:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "selected")
                default:
                    break;
                }
            }
        }else if enrollmentDate <= LocalDate {
            let idxPath: IndexPath = IndexPath(row: self.indexPathRow, section: 0)
            if let cell: ExpandCell = self.feedbackTableView.cellForRow(at: idxPath) as? ExpandCell {
                
                
                
                let text = self.afterCourse[self.indexPathRow]
                self.userDict[text] = sender.tag
                //Saving the data in locally
                UserDefaults.standard.set(self.userDict as Any, forKey: self.userDefaultKey)
                print(self.userDict)
                
                switch sender.tag {
                case 1001:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "selected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    feedback.feedback =  cell.feedBack1.text
                case 1002:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "selected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "unSelected")
                    feedback.feedback =  cell.feedBack2.text
                case 1003:
                    cell.feedbackExcellentImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackGoodImageView.image = #imageLiteral(resourceName: "unSelected")
                    cell.feedbackBadImageView.image = #imageLiteral(resourceName: "selected")
                default:
                    break;
                }
            }
        }
        
        
        
        if feedback.question == nil {
            print("question nill")
        } else  {
            
            feedbackRequest.studentFeedbackDetailsDto.append(feedback)
        }
        
    }
    
    @IBAction func submitAct(_ sender: UIButton) {
        
        
        SingleToneClass.shared.showProgressLoading(title: "Submiting Please Wait...")
        
        
        print(feedbackRequest)
        
        if feedbackRequest.studentId == "" {
            
            print("student id nil")
            
        } else if feedbackRequest.studentFeedbackDetailsDto.count < 4 {
            SingleToneClass.shared.dismissProgressLoading()
            SingleToneClass.shared.showValidationAlert(target: self, title: "Answer All Questions!", message: "") { }
            
        }else if feedbackRequest.studentFeedbackDetailsDto.count >= 4 {
            
           
            
            NetWorkingManager.sendFeedBack(feedback: feedbackRequest, listener: { (result,success) in
                
                print(self.feedbackRequest)
                
                if let result  = result as? FeedBackResponseModel {
                    
                    
                    
                    if result.error ==  "0" {
                        
                        SingleToneClass.shared.dismissProgressLoading()
                        SingleToneClass.shared.showValidationAlert(target: self, title: "Sucessfully Submitted", message: "") {
                            
                            self.feedBackOut.isHidden = true
                            self.dismiss(animated:true, completion: {
                                
                               
                                
                            })
                            
                        }
                    }else{
                         SingleToneClass.shared.dismissProgressLoading()
                        SingleToneClass.shared.showValidationAlert(target: self, title: "FeedBack Already Submitted.", message: "") {
                            
                            self.dismiss(animated:true, completion: {
                                
                            })
                            
                        }
                        
                        
                        print("Already Submitted")
                    }
                    
                } else {
                    print(Error.self)
                }
            })
        }
    }
}

