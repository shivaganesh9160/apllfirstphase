//
//  CollapseCell.swift
//  FeedbackTest
//
//  Created by SujiRaj on 15/11/18.
//  Copyright © 2018 MPME. All rights reserved.
//

import UIKit

class CollapseCell: UITableViewCell {

    @IBOutlet weak var feedbackTextLabel: UILabel!
    @IBOutlet weak var feedbackArrowImageView: UIImageView!
    @IBOutlet weak var feedbackContentViewButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
