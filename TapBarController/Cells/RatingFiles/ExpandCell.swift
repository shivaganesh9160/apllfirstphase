//
//  ExpandCell.swift
//  FeedbackTest
//
//  Created by SujiRaj on 15/11/18.
//  Copyright © 2018 MPME. All rights reserved.
//

import UIKit

class ExpandCell: UITableViewCell {

    @IBOutlet weak var feedbackTextLabel: UILabel!
    @IBOutlet weak var feedbackExcellentImageView: UIImageView!
    @IBOutlet weak var feedbackGoodImageView: UIImageView!
    @IBOutlet weak var feedbackBadImageView: UIImageView!
    @IBOutlet weak var excellentViewButton: UIButton!
    @IBOutlet weak var goodViewButton: UIButton!
    @IBOutlet weak var badViewButton: UIButton!
    @IBOutlet weak var feedbackHeaderButton: UIButton!
    @IBOutlet weak var feedBack1: UILabel!
    @IBOutlet weak var feedBack2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        badViewButton.isHidden = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    
    
}
