//
//  EventsTableViewCell.swift
//  AutoResized@Apll
//
//  Created by synycs on 16/11/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit
import SkeletonView
class EventsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var eventsMainImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var datesForEvents: UILabel!
    @IBOutlet weak var mainDescriptionLbl: UILabel!
    @IBOutlet weak var descTextLabel: UILabel!
    @IBOutlet weak var downArrowButton: UIButton!
    @IBOutlet weak var CVConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
         
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection:.topLeftBottomRight)

        let gradient = SkeletonGradient(baseColor:UIColor.lightGray)
        eventsMainImg.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
 
        self.downArrowButton.isHidden = true
        self.dividerView.isHidden = true
    }
    
    
    func hideAnimation(){
        
        eventsMainImg.hideSkeleton()
}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

         
    }
    
}
