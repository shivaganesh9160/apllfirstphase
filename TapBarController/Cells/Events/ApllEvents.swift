//
//  ApllEvents.swift
//  AutoResized@Apll
//
//  Created by synycs on 16/11/18.
//  Copyright © 2018 synycs. All rights reserved.
//


import UIKit
import SVProgressHUD
import Alamofire

class ApllEvents: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var eventTableView: UITableView!
    var selectedIndex: Int = -1
    
    var expandText = "The Great Escape Water Park has everything you need and more for an amazing time in Mumbai nearby to Thane. Our water park features an all inclusive price so you can feel confident knowing that your water park tickets give you the maximum amount of value. Browse our website to see our water park packages so you can choose the one that is best for you. As one of the best water parks in the local area, you are guaranteed to have an amazing time!"
    
    var collapseText = "The Great Escape Water Park has everything you need and more for an amazing time in Mumbai nearby to Thane. Our water park features an all"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventTableView.estimatedRowHeight = 150
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if eventsNames.count != 0 {
            
            return  eventsNames.count
        }else {
            
            
            return 10
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "EventsTableViewCell", bundle: nil), forCellReuseIdentifier: "EventsCell")
        
        if let cell:  EventsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as?  EventsTableViewCell
            
        {
             
            if  eventsNames.count != 0 {
                
//                self.eventTableView.reloadData()
                cell.hideAnimation()
                cell.downArrowButton.isHidden = false
                cell.dividerView.isHidden = false
                let dataDecoded : Data = Data(base64Encoded:  imageString[indexPath.row], options: .ignoreUnknownCharacters)!
                let decodedimage = UIImage(data: dataDecoded)
                
                cell.eventsMainImg.image = decodedimage
                cell.descriptionLbl.text = eventsNames[indexPath.row]
//                print(eventsNames)
                
                let does =   eventDates[indexPath.row]
                let myDate = does
                let dateFormatter = DateFormatter()
                
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date = dateFormatter.date(from:myDate)!
                dateFormatter.dateFormat = "d MMM\nyyyy"
                let dateString = dateFormatter.string(from:date)
                
                cell.datesForEvents.text =  dateString
                cell.locationLbl.text =  locationStr[indexPath.row]
                cell.mainDescriptionLbl.text =  eventsDescription[indexPath.row]
                
            }
            
            cell.downArrowButton.layer.cornerRadius = cell.downArrowButton.frame.width / 2
            cell.downArrowButton.tag = indexPath.row
            
            if  eventsNames.count != 0 {
                
                
                if self.selectedIndex == indexPath.row {
                    
//                    cell.descTextLabel.adjustsFontSizeToFitWidth =   false
//                    cell.descTextLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
 
                    let localDisc = eventsDescription[indexPath.row]
                    
                    let hei = self.heightForView(text:    eventsDescription[indexPath.row] , font: UIFont(name: "Helvetica Neue", size: 18)!, width: cell.descTextLabel.frame.width)
                    
                    cell.CVConstraint.constant = hei  
                    cell.descTextLabel.text =   eventsDescription[indexPath.row]
                    cell.downArrowButton.setImage(UIImage(named: "upBtn"), for: .normal)
                }
                else {

                  cell.descTextLabel.adjustsFontSizeToFitWidth =  false
                    let descriptionRef =  eventsDescription.prefix(40)
                    print("faffa\(descriptionRef)")
                    print(eventsDescription)

                    let hei = self.heightForView(text:  initialDesc[indexPath.row]  , font: UIFont(name: "Helvetica Neue", size: 18)!, width: cell.descTextLabel.frame.width)

                    cell.CVConstraint.constant = hei  + 45
                    cell.descTextLabel.text = initialDesc[indexPath.row]

                    cell.downArrowButton.setImage(UIImage(named: "downBtn"), for: .normal)
                }
               
                cell.downArrowButton.addTarget(self, action: #selector(ApllEvents.downArrowTapped(_:)), for: .touchUpInside)
                
            }
            
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func downArrowTapped(_ sender: UIButton) {
        if self.selectedIndex != sender.tag {
            self.selectedIndex = sender.tag
        }else{
            self.selectedIndex = -1
        }
        self.eventTableView.reloadData()
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
        
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        
        
       
    }
    
   
}

