//
//  CollectionViewCell.swift
//  TapBarController
//
//  Created by synycs on 25/10/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var imgeView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
