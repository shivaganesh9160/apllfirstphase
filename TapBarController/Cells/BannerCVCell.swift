//
//  BannerCVCell.swift
//  AutoResized@Apll
//
//  Created by synycs on 13/11/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit
import SkeletonView
class BannerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerView: GradientView!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var bannerDate: UILabel!
    @IBOutlet weak var bannerTitle: UILabel!
    @IBOutlet weak var bannerLocation: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection:.topLeftBottomRight)
        let gradient = SkeletonGradient(baseColor:UIColor.lightGray)

        bannerImg.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
       
    }
    
    func hideAnimation(){
        
        bannerImg.hideSkeleton()
 
    }
    
  
}
