//
//  ViewController2.swift
//  TapBarController
//
//  Created by synycs on 23/10/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import UIKit
import SystemConfiguration
class ViewController2: UIViewController  {
    
    
    @IBOutlet weak var loginBtnOut: UIButton!
    
    @IBOutlet weak var signUpBtnOut: UIButton!
    
    var loginRef = LogInModel()
    
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SingleToneClass.shared.isInternetAvailable()==false{
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "No Internet Connection", message:  "") {
                
                self.emailTF.becomeFirstResponder()
                
            }
            
        }
        //    self.loginBtnOut.layer.cornerRadius = self.loginBtnOut.bounds.size.width / 2.0
        //    self.signUpBtnOut.layer.cornerRadius = self.signUpBtnOut.bounds.size.width / 2.0
        
        self.hideKeyboardWhenTappedAround()
        emailTF.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        let image = UIImage(named: "user-profile-icon white 2")
        imageView.image = image
        emailTF.leftView = imageView
        
        passwordTF.leftViewMode = UITextField.ViewMode.always
        let imageView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 15 , height: 15))
        let image2 = UIImage(named: "icon_sets_online_shopping_fill_iconfinder-30-512 2")
        imageView2.image = image2
        passwordTF.leftView = imageView2
        
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
//    
//    
    @IBAction func logInBtn(_ sender: Any) {
        
        if SingleToneClass.shared.isInternetAvailable()==true {
         
        if   (emailTF.text?.count)!  <= 1 {
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "Alert !", message: "Please enter Valid User Name") {
                self.emailTF.becomeFirstResponder()
            }
            
        }else if  (passwordTF.text?.count)!  <= 1 {
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "Alert !", message: "Please Enter Valid Password") {
                self.passwordTF.becomeFirstResponder()
                
            }
            
        } else if  (passwordTF.text?.count)! >= 1 {
            
            SingleToneClass.shared.showProgressLoading(title: "Loging In")
            
            loginRef.username = emailTF.text!
            loginRef.password = passwordTF.text!
            loginRef.student = "true"
            NetWorkingManager.sendLogInModel(sendLogin:loginRef) { (result: Any, success: Bool) in
                 let result  = result as? LoginResponceModel
                
//                print(result)
                
                if result?.user.employeeId != nil {
                    
                    SingleToneClass.shared.dismissProgressLoading()
                    
                    EmployeeIDFrom = result!.user.employeeId!
                    IDNumber = result!.user.userName!
                    
                    if result?.user.studentBatchEnrolledDate == nil {
                        
                        print("errorDateFromEnrollment")
                    }else{
                    
                    enrollmentDate = result!.user.studentBatchEnrolledDate!
                        
                        print("FFFFFFFFFF\(enrollmentDate)")
                    }
                    print(EmployeeIDFrom)
                    print(IDNumber)
                     
                    var homeRef = Terms_Conditions()
                    homeRef = self.storyboard?.instantiateViewController(withIdentifier: "Term") as!  Terms_Conditions
                    self.present(homeRef, animated: true) {
                        SingleToneClass.shared.dismissProgressLoading()
                    }
                }
                
            }
        }
            
            
        }else{
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "No Internet Connection", message:  "") {
                
                
                
                self.emailTF.becomeFirstResponder()
                
                
            }
            
        }
    }
     
    
    @IBAction func signUpBtn(_ sender: Any) {
        
        var  signUpRef =  SignUpPage()
        
        signUpRef = self.storyboard?.instantiateViewController(withIdentifier: "signUp") as! SignUpPage
        
        self.present(signUpRef, animated: true) {
            
        }
       
    }
     
    
}
 
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}




