//
//  StudentAddressUpdate.swift
//  Apll
//
//  Created by synycs on 29/01/19.
//  Copyright © 2019 synycs. All rights reserved.
//

import UIKit

class AddressUpdate: UIViewController {

    
    @IBOutlet weak var areaTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var streetTF: UITextField!
    @IBOutlet weak var doorTF: UITextField!
    @IBOutlet weak var distTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            
    
        if  AreaGref.count == 0 {

            
            
         print(Error.self)

        }else{
            
            self.areaTF.text =  AreaGref 
         
            print(self.areaTF)
            
        }
        if  cityGref.count != 0 {

            self.cityTF.text =  cityGref
        }else{
            print(Error.self)
            
        }

        if StateGref.count != 0 {

            self.stateTF.text =  StateGref

        }else{
            
            print(Error.self)
        }

        if  StateGref.count != 0 {

            self.streetTF.text =  StateGref

        }else{
             print(Error.self)
            
        }

        if  doorGref.count != 0 {
            self.doorTF.text =  doorGref
        }else{
            
           print(Error.self)
        }

        if DistrictGref.count  != 0 {
            self.distTF.text =  DistrictGref

        }else{
            
            print(Error.self)
        }

        }
    }
    
    @IBAction func uploadBtn(_ sender: Any) {
        
        let areaTextField = areaTF.text!
        let cityTextField = cityTF.text!
        let stateTextField = stateTF.text!
        let employeeID = EmployeeIDFrom
        let dist = distTF.text!
        let doorNo = doorTF.text!
        let street = streetTF.text!
        
        if cityTF.text == stateTF.text {
            
            SingleToneClass.shared.showValidationAlert(target: self, title:  "Alert", message: "Both Numbers Should'nt Be Same") {
                
                self.cityTF.becomeFirstResponder()
                
            }
            
        }else {
         
            
            
            Service.shared.putMethod(params: ["area":"\(areaTextField)","city":"\(cityTextField)","state":"\(stateTextField)","district":"\(dist)","doorNo":"\(doorNo)","streetName":"\(street)"], url: "http://174.138.121.216/student//student/updateStudentAddress/\(employeeID)"){ (response) -> (Void) in
           
            
                let responce = response as! NSDictionary
                print(responce)
                
                 let error = responce["error"] as? Bool
                
                print(error!)
                
                    if error  ==  false {
                
                
            SingleToneClass.shared.showValidationAlert(target: self, title: "Alert", message:  "Successfully Updated.", OntapOkButton: {
                
               var nextVC = Tab()
                
                nextVC = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! Tab
                
                
                self.present(nextVC, animated: true, completion: {
                    
                    self.tabBarController?.tabBar.isHidden = false
                    
                })
                
                
            })
                }else{
                    
               SingleToneClass.shared.showValidationAlert(target: self, title: "Alert", message:  "Failed!", OntapOkButton: {
                   
                    
                    
                })
                        
                    }
 
      
        }
            
        }
    }
 
    @IBAction func backBtn(_ sender: Any) {
        
        self.dismiss(animated: true) {
           
        }
        
    }
    

}


class MobileNumberUpdate: UIViewController {
    
    @IBOutlet weak var oldMblNumTF: UITextField!
    @IBOutlet weak var newMblTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func numberUpdateBtn(_ sender: Any) {
        let oldMblNum =  oldMblNumTF.text!
        let newMblNum = newMblTF.text!
        let employeeID = EmployeeIDFrom
        
        
        
        if oldMblNumTF.text == " " && newMblTF.text == " " {
        
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "Fields Shouldn't Be Empty", message: " ") {
                
                
                self.oldMblNumTF.becomeFirstResponder()
                
            }
            
            
        } else if oldMblNumTF.text == newMblTF.text {
            
            SingleToneClass.shared.showValidationAlert(target: self, title:  "Alert", message: "Both numbers shouldn't be same") {
                
                self.oldMblNumTF.becomeFirstResponder()
                
            }
            
            
        }else if (newMblTF.text?.count)! >= 10 && (oldMblNumTF.text?.count)! >= 10 {
        
        
            Service.shared.putMethod(params:["newMobileNumber":"\(newMblNum)","oldMoileNumber":"\(oldMblNum)","studentId":"\(employeeID)"] , url: "http://174.138.121.216/student/student/updateMobileNumber") { (response) -> (Void) in
                
                
                let responce = response as! NSDictionary
                print(responce)
                
                let error = responce["error"] as? Bool
                let errorMsg = responce["errorMsg"] as? String
                print(error!)
                
                if error  ==  false {
                    
                    
                    SingleToneClass.shared.showValidationAlert(target: self, title: "Alert", message:  "Successfully Updated.", OntapOkButton: {
                        
                        var nextVC = Tab()
                        
                        nextVC = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! Tab
                        
                        
                        self.present(nextVC, animated: true, completion: {
                            
                            self.tabBarController?.tabBar.isHidden = false
                            
                        })
                        
                        
                    })
                }else{
                    
                    
                    let aString =  errorMsg!
                    let newString = aString.replacingOccurrences(of: "[", with: " ")
                    let finalString = newString.replacingOccurrences(of: "]", with: " ")
                    
                    print(newString)
                    
                    SingleToneClass.shared.showValidationAlert(target: self, title:"\(finalString)" , message: "", OntapOkButton: {
                        
                        self.oldMblNumTF.becomeFirstResponder()
                        
                    })
                    
                }
                
                
            }
        }else{
            
            
            
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Enter Valid Mobile Number!") {
                
                
                self.newMblTF.becomeFirstResponder()
                
                
                
            }
            
            
            
            
            
            
            
            
        }
    
    }
    
    
    
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
        }
        
    }
    
    
}
 
class  changePassword: UIViewController {
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    
    @IBOutlet weak var oldPassTF: UITextField!
    @IBOutlet weak var newPassTF: UITextField!
    @IBAction func changePassBtn(_ sender: Any) {
      
    let oldPass =  oldPassTF.text!
    let newPass =  newPassTF.text!
    let employeeID = EmployeeIDFrom
        
        
        
        
        
        
        if oldPassTF.text == " " && newPassTF.text == " "{
            
            SingleToneClass.shared.showValidationAlert(target: self, title: "Both Fields Shouldn't Be Empty", message:  "") {
                
                self.oldPassTF.becomeFirstResponder()
                
            }
            
            
        }else if oldPassTF.text == newPassTF.text {
            
            SingleToneClass.shared.showValidationAlert(target: self, title:  "Alert", message: "Both Numbers Should'nt Be Same") {
                
                self.oldPassTF.becomeFirstResponder()
                
            }
            
            
        }else if (newPassTF.text?.count)! >= 2 && (oldPassTF.text?.count)! >= 2 {
         
            Service.shared.putMethod(params:["newPassword":"\(newPass)","oldPassword":"\(oldPass)","studentId":"\(employeeID)"] , url: "http://174.138.121.216/student/student/updatePassword"){ (response) -> (Void) in
                
                
                let responce = response as! NSDictionary
                print(responce)
                
                let error = responce["error"] as? Bool
                let errorMsg = responce["errorMsg"] as? String
                print(error!)
                
                if error  ==  false {
                    
 
                    
                    SingleToneClass.shared.showValidationAlert(target: self, title: "Alert", message:  "Successfully Updated.", OntapOkButton: {

                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                exit(0)
                            }
                        }
                         
//                        var nextVC = Tab()
//
//                        nextVC = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! Tab
//
//
//                        self.present(nextVC, animated: true, completion: {
//
//                            self.tabBarController?.tabBar.isHidden = false
//
//                        })


                    })
                }else{
                    
                    SingleToneClass.shared.showValidationAlert(target: self, title: "Alert", message:  "\(errorMsg!)", OntapOkButton: {
                        
                        
                        
                    })
                    
                }
                
                
            }
        }else{
            
            SingleToneClass.shared.showValidationAlert(target:self, title: "Alert!", message:"Please Fill Both Fields!") {
                
                self.newPassTF.becomeFirstResponder()
                
            }
          
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
class  mainPageForUpdateDetails: UIViewController {
     
    @IBAction func backBtn(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            
        }
        
    }
    
    @IBAction func addressUpdateBtn(_ sender: Any) {
        
        var addressUpdatePage = AddressUpdate()
        
        addressUpdatePage = self.storyboard?.instantiateViewController(withIdentifier: "addressUpdateVC") as! AddressUpdate
        
        
        self.present(addressUpdatePage, animated: true) {
            
           
        }
       
        
    }
    
    @IBAction func changePassBtn(_ sender: Any) {
        
        
        
        var changePasswordVC = changePassword()
        
        changePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "changePasswordVC") as!  changePassword
        
        
        self.present(changePasswordVC, animated: true) {
            
           
            
        }
        
        
    }
    
    @IBAction func mobileNumBtn(_ sender: Any) {
        
        
        var mobileNumberVC = MobileNumberUpdate()
        
        mobileNumberVC = self.storyboard?.instantiateViewController(withIdentifier: "mobileNumberUpdateVC") as!  MobileNumberUpdate
        
        
        self.present(mobileNumberVC, animated: true) {
          
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
     
}

