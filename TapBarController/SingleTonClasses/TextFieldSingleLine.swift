//
//  TextFieldSingleLine.swift
//  Apll
//
//  Created by synycs on 27/12/18.
//  Copyright © 2018 synycs. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class TextStyle: UITextField {
    
    @IBInspectable var cornercolor:UIColor = .white
    @IBInspectable var boarderwidth:CGFloat = 0.0
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = cornercolor.cgColor
        
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = boarderwidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
}
