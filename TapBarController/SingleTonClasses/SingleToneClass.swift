//
//  SingleToneClass.swift


import UIKit
import SVProgressHUD
import SystemConfiguration
class SingleToneClass: NSObject {
    
    static let shared = SingleToneClass()
    
    override init() {
        
    }
     
    func showValidationAlert(target : UIViewController, title : String, message : String, OntapOkButton : @escaping (() -> Void))  {
        
        let emailAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (okayAction) in
            
            OntapOkButton()
            
        }
        emailAlert.addAction(okAction)
        DispatchQueue.main.async {
            target.present(emailAlert, animated: true, completion: nil)
        }
        
    }
     
    func showProgressLoading(title : String) {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.setForegroundColor(#colorLiteral(red: 0.09019607843, green: 0.2039215686, blue: 0.337254902, alpha: 1))
            SVProgressHUD.show(withStatus: "  \(title)  ")
        }
    }
    
    func dismissProgressLoading() {            DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func dismissProgressLoadingWithSucess(message : String) {
        DispatchQueue.main.async {
            SVProgressHUD.showSuccess(withStatus: message)
        }
    }
    
    func dismissProgressLoadingWithError(message : String) {
        DispatchQueue.main.async {
            SVProgressHUD.showError(withStatus: message)
        }
    }
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
