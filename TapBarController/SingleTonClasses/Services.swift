//
//  shared.swift
//  json using singleton

//  Copyright © 2018 Arun Kumar Reddy. All rights reserved.
//

import Foundation

class Service {
    
   static let shared=Service()
    init() {
        
    }
    
    let baseUrl="http://174.138.121.216/student"
 
    func GETService(extraParam : String, onTaskCompleted : @escaping (Any)->(Void) ) {
        
        guard let url = URL(string: "\(baseUrl)/\(extraParam)") else { return }
        print(url)
        
        var urlRequest = URLRequest(url: url)
         urlRequest.addValue( "text/html;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if error != nil {
                print(error as Any)
            } else {
                do {
                    guard let dataIs = data else { return }
                    let serverResponse = try JSONSerialization.jsonObject(with: dataIs, options:.allowFragments)
                    
                    onTaskCompleted(serverResponse)
                    
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func POSTService(serviceType : String, postString : String, onTaskCompleted : @escaping (Any)->(Void)) {

        guard let url = URL(string: "\(baseUrl)/\(serviceType)") else { return }
        print(url)
        print(postString)
        
        var urlRequest = URLRequest(url: url)

        urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postString.data(using: .utf8)

        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                print(error as Any)
            } else {
                do {
                    guard let dataIs = data else { return }
                    let serverResponse = try JSONSerialization.jsonObject(with: dataIs, options: .allowFragments)
                    onTaskCompleted(serverResponse)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    
    func putMethod(params:[String:Any],url:String,onTaskCompleted : @escaping (Any)->(Void)){
        
//    let params: [String: Any] = ["username":"SY-45", "password":"Nagaraju@123"]
        
        let session: URLSession = URLSession(configuration: URLSessionConfiguration.default)
        
        var urlRequest = URLRequest(url: URL(string: url)! )
        
        urlRequest.httpMethod = "PUT"
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonTodo: Data
        
        do {
            
            jsonTodo = try JSONSerialization.data(withJSONObject: params, options: [])
            
            urlRequest.httpBody = jsonTodo
            
        } catch {
            
            print("Error: cannot create JSON from todo")
            
            return
            
        }
        
        let dataTask = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            do {
                
                let token = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                onTaskCompleted(token)
//                print(token)
//
//                UserDefaults.standard.set(token, forKey: "authToken")
//                
//                print(token)
//
            } catch {
                
                print("error")
                
            }
            
        })
        
        dataTask.resume()
         
    }
    
}
    
    
    
    
    
    
    
    


